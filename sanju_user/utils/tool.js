export function backHandler() {
    const pages = getCurrentPages()
    if (pages.length > 1) {
        return wx.navigateBack()
    }
    wx.switchTab({
        url: '/pages/index/index'
    })
}

export function toast({title, time = 3000, icon = 'none'}, ...arg) {
    if (!title) return
    wx.showToast({
        title,
        icon,
        duration: time
    })
    const [callback] = arg
    if (!!callback) {
        let timeID = setTimeout(() => {
            clearTimeout(timeID)
            timeID = null
            callback()
        }, time)
    }
}

export function wxPay(options) {
    return new Promise(resolve => {
        wx.requestPayment({
            ...options,
            success: () => {
                resolve([null, true])
            },
            fail: error => {
                resolve([error, null])
            }
        })
    })

}

export function uploadFile() {
    return new Promise(resolve => {
        wx.uploadFile({
            url: 'https://sanju.xinyunweb.com/api/newupload/postImage',
            name: 'file',
            header: {
                token: ''
            }
        })
    })
}

export function cloneDataFunc(data) {
    const result = {}
    for (const key in data) {
        result[key] = data[key]
    }
    return result
}

export function delay(time = 300) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(time)
        }, time)
    })

}
