import {toast} from "../../utils/tool";

const app = getApp();
Page({
    data: {
        mobile: '',
        userInfo:{}
    },
    getUserInfo(){
        app.ajax({
            url: '/api/user/userinfo',
            success: res => {
                const {code,data} = res
                if(code !== 1) return
                app.globalData.userInfo = data
                wx.setStorageSync('user', data)
                wx.setStorageSync('token', data.token);
            }
        })
    },
    getPhoneNumber(ev) {
        app.ajax({
            url: '/api/user/loginwxa',
            data: {
                code: ev.detail.code
            },
            success: resp => {
                console.log(resp,'resp')
                const {code,  data} = resp
                if (code === 1 && !!data) {
                    this.getUserInfo()
                    toast({title:'绑定成功',},()=>{
                        wx.navigateBack()
                    })
                }
            }
        })
    },

    onLoad: function(options) {
        const userInfo = app.globalData.userInfo
        console.log(userInfo,'userInfo')
        this.setData({
            userInfo
        })
    },

})