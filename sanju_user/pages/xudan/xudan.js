import EventBus from "../../utils/eventBus";
import {toast} from "../../utils/tool";

const app = getApp();
Page({
    data: {
        room_id: null,
        room_data: [],
        selectIndex: 1,
        order_id: 0,
        money: '',
        type: 1,
        loading: false,

    },
    toAccount() {
        wx.navigateTo({
            url: "/pages/group-buying/group-buying?event=account"
        })
    },

    selectType(ev) {
        const {type} = ev.currentTarget.dataset
        if (this.data.type === type) return
        this.setData({
            type
        })
    },
    initAccount(data) {
        const {money} = data
        this.setData({
            money,
        })
    },
    onUnload() {
        EventBus.$off('accountSuccess', this.initAccount)
    },

    onLoad(options) {
        EventBus.$on('accountSuccess', this.initAccount)
        const {globalData: {userInfo: {money}}} = app
        const {room_id, order_id} = options
        this.setData({
            room_id: room_id,
            order_id: order_id,
            money,
        })
    },

    selectTime(e) {
        this.setData({
            selectIndex: e.currentTarget.dataset.index
        })
    },
    changepassword() {
        app.ajax({
            url: '/api/user/getRoomInfo',
            data: {
                roomId: this.data.room_id
            },
            success: res => {
                this.setData({
                    room_data: res.data,
                })

            }
        });
    },
    updateUserInfo() {
        app.updateUserInfo()
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.changepassword()
    },
    pay() {
        const hasMobile = app.checkHasMobile()
        if (!hasMobile) return
        if (this.data.loading) return
        this.data.loading = true
        let that = this;
        const {type} = this.data
        app.ajax({
            url: '/api/user/renewalOrder',
            data: {
                orderId: that.data.order_id,
                roomId: that.data.room_id,
                time: that.data.selectIndex,
                pay_type: type,
            },
            success: res => {
                if (res.code === 1) {
                    if (!!res.data && res.data.hasOwnProperty('paySign')) {
                        return wx.requestPayment({
                            nonceStr: res.data.nonceStr,
                            package: res.data.package,
                            paySign: res.data.paySign,
                            timeStamp: res.data.timeStamp,
                            signType: res.data.signType,
                            fail: () => {
                                toast({title: '支付失败'}, () => {
                                    this.data.loading = false
                                })
                            },
                            success: () => {
                                if (type === 2) {
                                    this.updateUserInfo()
                                }
                                toast({
                                    title: '续单成功',
                                    icon: 'success',
                                }, () => {
                                    this.data.loading = false
                                    wx.switchTab({
                                        url: '/pages/index/index'
                                    })
                                })
                            }
                        })
                    }
                    const {msg} = res
                    if (type === 2) {
                        this.updateUserInfo()
                    }
                    toast({title: msg}, () => {
                        wx.switchTab({
                            url: '/pages/index/index'
                        })
                        this.data.loading = false
                    })
                    return
                }
                toast({title: res.msg})
                this.data.loading = false
            }
        }, 1);
    },
})
