import {toast} from "../../../utils/tool";

const app = getApp()
Component({
    properties: {
        // list: {
        //     type: Array,
        //     value: [],
        // },
        type: {
            type: Number,
        },
        current: {
            type: Number,
            observer: function (newVal, oldVal) {
                if (newVal === this.data.default) {
                    console.log('initData')
                }
            }
        },
        default: {
            type: Number
        },
        list: {
            type: Array,
        }
    },
    observers: {},
    pageLifetimes: {},
    lifetimes: {
        // ready() {
        //     const {type} = this.data
        //     this.initData(type)
        // }
    },
    data: {
        id: '',
        sexEnum: {
            1: "男",
            2: '女'
        },
        // list:[]
    },
    methods: {
        initData(job_type) {
            app.ajax({
                url: '/api/user/teaMasterList',
                data: {job_type},
                success: res => {
                    const {code, data, msg} = res
                    if (code !== 1) {
                        return toast({title: msg})
                    }
                    const {list: {data: record, per_page, current_page, total}} = data
                    const hasMore = current_page * per_page < total
                    this.setData({
                        list: [...record]
                    })
                    //
                    // if (!clear) {
                    //     const {list: _list} = this.data
                    //     _list.push(...record)
                    //     this.setData({
                    //         list: [..._list],
                    //     })
                    // } else {
                    //     this.setData({
                    //         list: [...record]
                    //     })
                    // }
                }
            });
        },

        yuyue(ev) {
            const {id, status} = ev.currentTarget.dataset
            if (!status) return toast({title: '不可预约'})
            this.setData({
                id,
            })
            wx.navigateTo({
                url: '/pages/yuyue/yuyue?id='+ id
            })
        },
    }
});
