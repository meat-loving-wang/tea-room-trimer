import {toast} from "../../utils/tool";

const app = getApp();
Page({
    data: {
        active: 0,
        list: [],
        sexEnum: {
            1: "男",
            2: '女'
        },
        menus: [
            {label: "全部", icon: "icon-0.png", value: 0},
            {label: "茶艺师", icon: "icon-1.png", value: 1},
            {label: "台球助教", icon: "icon-2.png", value: 2},
            {label: "游玩", icon: "icon-3.png", value: 3},
            {label: "观影", icon: "icon-4.png", value: 4},
            {label: "商务", icon: "icon-5.png", value: 5},
        ],
        banner: [],
        id: '',
        location: {
            lng: '',
            lat: '',
        }
    },
    btnClick(ev) {
        const {type} = ev.currentTarget.dataset
        if (type === 'order') return wx.navigateTo({
            url: "/pages/cha-order/cha-order"
        })
        return wx.navigateToMiniProgram({
            appId: 'wxc4c033f5c53f00c5',
            path: '/pages/login/login', //路径和携带的参数
            envVersion: 'release',
        })

    },
    choseCategory(ev) {
        const {index} = ev.currentTarget.dataset
        if (this.data.active === index) return
        this.setData({
            active: index
        })
        this.initData(true)
    },
    onChange(event) {
        this.setData({
            active: event.detail.index
        })
        this.initData(true)
    },
    yuyue(e) {
        this.setData({
            id: e.currentTarget.dataset.id
        })
        wx.navigateTo({
            url: '/pages/yuyue/yuyue?id=' + e.currentTarget.dataset.id
        })
    },
    swiperChange(ev) {
        const {current} = ev.detail
        this.setData({
            active: current,
        })
    },
    getBanner() {
        app.ajax({
            url: '/api/banner/bannerlist',
            data: {
                type: 2
            },
            success: res => {
                this.setData({
                    banner: res.data
                })
            }
        })
    },
    toNotice(ev) {
        const {item} = ev.currentTarget.dataset
        const {url} = item
        if (!url) return
        wx.navigateTo({
            url: '/pages/web-view/web-view?url=' + encodeURIComponent(url)
        })
    },
    initData(clear = false) {
        wx.showLoading({
            title: '正在获取数据'
        })
        const {active, location} = this.data
        const {lon, lat} = location
        const job_type = active
        const payload = {
            user_lon: lon, user_lat: lat
        }
        if (active !== 0) {
            payload.job_type = job_type
        }
        app.ajax({
            url: '/api/user/teaMasterList',
            data: payload,
            success: res => {
                const {code, data, msg} = res
                if (code !== 1) {
                    return toast({title: msg})
                }
                let {list: {data: record, per_page, current_page, total}} = data
                const hasMore = current_page * per_page < total
                wx.hideLoading()
                record = record.map(item => {
                    const {work_state, avatar} = item
                    if (!avatar.includes('https')) {
                        item.avatar = `https://sanju.xinyunweb.com${avatar}`
                    }
                    return {...item, work_state: Boolean(Number(work_state))}
                })
                if (!clear) {
                    const {list: _list} = this.data
                    _list.push(...record)
                    this.setData({
                        list: [..._list],
                    })
                } else {
                    this.setData({
                        list: [...record]
                    })
                }
            }
        });
    },
    onLoad: function (options) {
        const location = wx.getStorageSync('location')
        this.getBanner()
        if (!!location) {
            this.data.location = {...location}
            this.initData()
        } else {
            wx.getLocation({
                success: res => {
                    this.data.location = {lat: res.latitude, lon: res.longitude}
                    wx.setStorageSync('location', this.data.location);
                    this.initData()
                },
                fail: () => {
                    toast({title: '获取位置失败'})
                }
            })
        }
    }
});
