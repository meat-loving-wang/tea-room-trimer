Page({
    data: {
        active: 0,
    },
    onChange(event) {
        const {dataset: {type}} = event.currentTarget
        const KEY_ENUM = {
            swiper: 'current',
            tab: 'index'
        }
        const key = KEY_ENUM[type]
        const active = event.detail[key]
        this.setData({
            active
        })
    },
    onLoad: function (options) {

    },
});