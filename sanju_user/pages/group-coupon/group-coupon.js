import {toast} from "../../utils/tool";
import {COUPON_ENUM} from "../../constant/index";

const app = getApp()
Page({
    data: {
        list: [],
    },
    onLoad: function (options) {
        const {
            globalData: {
                couponData
            }
        } = app
        this.setData({
            list: couponData[COUPON_ENUM.GROUP]
        })
    },
    initData() {
        app.ajax({
            url: '/api/coupon/couponlist',
            success: resp => {
                const {code, data, msg} = resp
                if (code !== 1) {
                    return toast({
                        title: msg
                    }, () => {
                        wx.navigateBack()
                    })
                }
                if (data && data.length) {
                    this.setData({
                        list: [...data.map(item => {
                            const createTime = this.formatDate(item['create_time'])
                            const expireTime = this.formatDate(item['expire_time'])
                            return {
                                ...item,
                                createTime,
                                expireTime
                            }
                        })]
                    })
                }
            }
        })
    },

});