Page({
    data: {
        baseURL: 'https://sanju.xinyunweb.com/h5',
        src: '',
    },
    onLoad: function (options) {
        const {baseURL} = this.data
        let src = ''
        if (options.hasOwnProperty('url')) {
            const {url} = options
            src = decodeURIComponent(url)
        } else {
            src = baseURL
        }
        console.log(src,'获取的src')
        this.setData({src})
    }
});