import {delay, toast} from "../../utils/tool";
import {BASE_URL} from "../../constant/index";

const app = getApp()
Page({
    data: {
        baseUrl: BASE_URL,
        storeList: [],
        showLocationPop: false,
        isEmpty: false,
        isScroll: false,
    },
    openDoor(store_id) {
        app.ajax({
            url: '/api/frontdoor/openfrontdoor',
            data: {},
            success: res => {
                wx.hideLoading()
                toast({title: res.msg})
            }
        })
    },
    getOrderList(status) {
        return new Promise(resolve => {
            app.ajax({
                url: '/api/user/orderList',
                data: {status, page: 1},
                success: res => {
                    resolve(res.data || [])
                }
            })
        })
    },
    async findStoreByOrder() {
        wx.showLoading({title: '加载中...', mask: true})
        await delay(300)
        this.openDoor()
        return

        const list1 = await this.getOrderList('30')
        const list2 = await this.getOrderList('31')
        const orderList = [...list1, ...list2]
        if (!orderList.length) {
            wx.hideLoading()
            return toast({title: '您没有预约订单,不能开门'})
        }
        const [firstOrder] = orderList
        const {store_id} = firstOrder
        const targetStore = this.data.storeList.find(item => item.id === store_id)
        if (!targetStore) {
            wx.hideLoading()
            return toast({title: '没有相关店铺'})
        }
    },
    scrollHandle() {
        if (this.timerId) {
            clearTimeout(this.timerId)
            this.timerId = null
        }
        this.setData({isScroll: true})
        this.timerId = setTimeout(() => {
            this.setData({isScroll: false})
        }, 300)
    },
    onSetting() {
        this.setData({
            showLocationPop: false,
        })
        this.getUserLocation()
    },
    checkOrderCache(id) {
        const {firstStore} = app.globalData
        if (!firstStore) return app.clearOrderCache()
        if (firstStore.id !== id) app.clearOrderCache()
    },
    changestore(e) {
        const {store} = e.currentTarget.dataset
        this.checkOrderCache(store.id)
        app.setFirst(store)
        app.globalData.indexType = 1
        app.globalData.selectStore = store
        toast({title: '选择成功'})
        setTimeout(() => {
            wx.switchTab({
                url: '/pages/index/index'
            })
        }, 1600)
    },
    // 打开地图
    openMap: function (e) {
        let mapdata = e.currentTarget.dataset.mapdata
        let arr = mapdata.lat.split(",")
        var lat = parseFloat(arr[1]),
            lon = parseFloat(arr[0]),
            firstStore = this.data.firstStore;
        if (!lat || !lon) {
            return false;
        }
        wx.openLocation({
            latitude: lon,
            longitude: lat,
            scale: 15,
            name: mapdata.name,
            address: mapdata.address,
            success: function (res) {
            },
            fail: function (res) {
            }
        })
    },
    getStore(e) {
        wx.showLoading({
            title: '正在获取门店'
        })
        app.ajax({
            url: "/api/room/storelist",
            data: {
                lat: e.lat,
                lon: e.lon
            },
            success: res => {
                const {code, data, msg} = res
                if (code !== 1) return toast({title: msg})
                this.setData({
                    storeList: data || [],
                    isEmpty: !data || !data.length
                })
                wx.hideLoading()
            }
        })
    },
    callPhone(e) {
        wx.makePhoneCall({
            phoneNumber: e.currentTarget.dataset.phone,
            success: function () {
            },
        })
    },
    getUserLocation() {
        wx.getLocation({
            type: 'gcj02',
            success: res => {
                const location = {
                    lat: res.latitude,
                    lon: res.longitude
                }
                app.setLocation(location)
                this.getStore(location)
            },
            fail: () => {
                this.getStore({lat: 0, lon: 0})
            }
        })
    },
    getLocationAuthorization() {
        wx.authorize({
            scope: 'scope.userLocation',
            success: () => {
                this.getUserLocation(false)
            },
            fail: () => {
                this.setData({
                    showLocationPop: true,
                })
            }
        })
    },
    onLoad(options) {
        this.getLocationAuthorization()
        app.getUserInfo()
    },
    reorderHandle() {
        if (!app.globalData.firstStore) {
            const [store] = this.data.storeList
            app.setFirst(store)
            app.globalData.indexType = 1
            app.globalData.selectStore = store
        }
        wx.navigateTo({
            url: '/pages/history_order/history_order'
        })
    },
    onShareAppMessage() {
        return {
            title: '博戏互娱',
            path: "/pages/store-list/store-list",
        }
    }
})
