const app = getApp();
const util = require('../../utils/util.js')
const {toast} = require("../../utils/tool");
Page({
    /**
     * 页面的初始数据
     */
    data: {
        service: false,
        lockHexId: '',
        lockId: '',
        _isNotifyReturn: false,
        lock_brand: true, //锁的类型标记判断是不是231型号的锁
        maxFailNums: 3,
        delayTime: 300,
        _failNums: 0,
        page: 1,
        pagesize: 15,
        list: [],
        is_ajax: false,
        is_over: false,
        status: 0,
        blueYaCode: '',
        // 门锁蓝牙的id
        deviceId: '',
        // 服务uuid
        servicesUUID: '',
        // 开启notify的characteristicId
        notifyCharacteristicId: '',
        // 开启write的characteristicId
        writeCharacteristicId: '',
        characteristics: []
    },

    cancelOrder(e) {
        wx.showModal({
            title: '您确定取消订单吗?',
            success: ({cancel}) => {
                if (cancel) return
                const {id: chayishi_order_id} = e.currentTarget.dataset
                app.ajax({
                    url: "/api/user/canalChayishiOrder",
                    data: {chayishi_order_id},
                    success: resp => {
                        // console.log(resp, '取消订单')
                        const {msg} = resp
                        toast({title:msg})
                        // if (code !== 1) return toast({title: msg})
                        this.getList(true)
                    }
                })
            }
        })

    },

    close(e) {
        let self = this;
        app.ajax({
            url: '/api/user/earlyClose',
            data: {
                order_id: e.currentTarget.dataset.id
            },
            success(res) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none',
                    duration: 800
                })
                self.getList(1)
            }
        })
    },

    // 转换
    ab2hex(buffer) {
        var hexArr = Array.prototype.map.call(new Uint8Array(buffer), function (bit) {
            return ('00' + bit.toString(16)).slice(-2);
        });
        return hexArr.join('');
    },


    clear() {
        this._failNums = 0;
        /* 设置traceId */
    },
    //字符串转arrbuffer
    stringToHexBuffer(value) {
        var typedArray = new Uint8Array(value.match(/[\da-f]{2}/gi).map(function (h) {
            return parseInt(h, 16);
        }))
        return typedArray.buffer;
    },

    // 判断机型
    isModel() {
        let res = wx.getSystemInfoSync();
        return res.res
    },

    unsettled: function (e) {
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/cys_order/cys_order?order_id=' + id
        })
    },
    opening_code: function (e) {
        var id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/cys_others/cys_orders?order_id=' + id
        })
    },
    used: function () {
        wx.navigateTo({
            url: '/pages/used/used',
        })
    },
    expired: function () {
        wx.navigateTo({
            url: '/pages/expired/expired',
        })
    },
    setStatus: function (e) {
        console.log(e)
        var status = e.currentTarget.dataset.status;
        if (this.data.status != status) {
            this.setData({
                status: status
            });
            this.getList(1);
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getList();
    },

    getList: function (reset) {
        if (reset) {
            this.setData({
                page: 1,
                list: [],
                is_ajax: false,
                is_over: false
            });
        }
        if (this.data.is_ajax || this.data.is_over) return false;
        this.setData({
            is_ajax: true
        });
        var page = this.data.page,
            status = this.data.status,
            list = reset ? [] : this.data.list,
            pagesize = this.data.pagesize,
            is_ajax = true,
            is_over = false;
        app.ajax({
            url: '/api/user/myMasterOrders',
            data: {
                order_type: status,
                page: page,
                pagesize: pagesize
            },
            success: res => {
                if (res.code == 1) {
                    page++;
                    list = list.concat(res.data.list.data);
                    var now = parseInt(Date.now() / 1000);
                    for (var i in list) {
                        if (list[i].status == 10) {
                            list[i].gopay_time = parseInt((list[i].create_time + 180 - now) /
                                60);
                            if (list[i].gopay_time < 0) list[i].gopay_time = 0;
                        }
                    }
                } else {
                    is_over = true;
                }
                is_ajax = false;
                this.setData({
                    page: page,
                    list: list,
                    is_ajax: is_ajax,
                    is_over: is_over
                });
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.getList();
    },
    onShareAppMessage: function (res) {
        console.log(res, "看一下")
        return {
            title: '亲 赠送给你一个茶室包间请 前去消费',
            path: `/pages/opening_code/opening_code?type=1&order_id=${res.target.dataset.id}`, //这里是被分享的人点击进来之后的页面
            imageUrl: '' //这里是图片的路径
        }
    },

})
