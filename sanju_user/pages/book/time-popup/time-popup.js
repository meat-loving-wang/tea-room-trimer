Component({
    properties: {
        show: {
            type: Boolean,
            value: false
        }
    },
    data: {
        timeColumns: []
    },
    lifetimes: {
        attached() {
            const result = []
            for (let i = 5; i < 25; i++) {
                result.push({
                    label: `${i}小时`,
                    value: i,
                })
            }
            this.setData({
                timeColumns: result
            })
        }
    },
    methods: {
        choseTimeCancel() {
            this.triggerEvent('cancel', false)
        },
        pickerConfirm(ev) {
            const {value} = ev.detail.value
            this.triggerEvent('confirm', value)
        },
    }
});
