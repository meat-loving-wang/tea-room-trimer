import {toast} from "../../../utils/tool";

Component({
    properties: {
        visible: {
            type: Boolean,
        },
        wifiName: {
            type: String
        },
        password: {
            type: String
        }
    },
    data: {},
    methods: {
        connectWifi: function () {
            //检测手机型号
            wx.getSystemInfo({
                success: res => {
                    let system = '';
                    if (res.platform === 'android') system = parseInt(res.system.substring(8));
                    if (res.platform === 'ios') system = parseInt(res.system.substring(4));
                    if (res.platform === 'android' && system < 6) {
                        return toast({title: '手机版本不支持'})
                    }
                    if (res.platform === 'ios' && system < 11.2) {
                        return toast({title: '手机版本不支持'})
                    }
                    //2.初始化 Wi-Fi 模块
                    this.startWifi();
                }
            })
        },
        //初始化 Wi-Fi 模块
        startWifi: function () {
            wx.startWifi({
                success: () => {
                    //请求成功连接Wifi
                    this.Connected();
                },
                fail: () => {
                    toast({title: '接口调用失败'})
                }
            })
        },

        Connected: function () {
            const {wifiName, password} = this.data
            wx.connectWifi({
                SSID: wifiName,
                BSSID: '',
                password,
                success: () => {
                    toast({title: "wifi连接成功", time: 2000})
                },
                fail: () => {
                    toast({title: 'wifi连接失败', time: 2000})
                }
            })
        },

        connection() {
            this.connectWifi()
        },
        onClose() {
            this.triggerEvent('close', false)
        },
        copyPass() {
            const {password} = this.data
            wx.setClipboardData({
                data: password,
                success: () => {
                    toast({title: '复制成功'})
                    this.onClose()
                }
            })
        },
    }
});
