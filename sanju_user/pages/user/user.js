import {STATUS_HEIGHT} from "../../constant/index";
import {toast} from '../../utils/tool'
import eventBus from "../../utils/eventBus";

const app = getApp();
Page({
    data: {
        wifi: {
            name: '',
            password: '',
        },
        showMobile: false,
        showWifi: false,
        statusHeight: STATUS_HEIGHT,
        couponLen: 0, groupLen: 0,
        menuList: [
            // {
            //     title: "团购验券",
            //     path: "/pages/group-buying/group-buying?type=3",
            //     image: '1',
            // },
            {
                title: "常见问题",
                path: "/pages/wenti/wenti",
                image: '2',
            }, {
                title: "WIFI连接",
                path: "",
                image: '3',
                type: "fn",
                name: 'wifi'
            }, {
                title: "全部门店",
                path: "/pages/changestore/changestore",
                image: '4',
            },
            {
                title: "我要加盟",
                path: "/pages/join/join",
                image: '5',
            },
            {
                title: "联系商家",
                path: "",
                image: '6',
                type: 'fn',
                name: 'mobile'
            },
            {
                title: "客服中心",
                path: "",
                type: 'fn',
                name: 'callPhone',
                image: '7',
            },
            // {
            //     title: "保洁中心",
            //     path: "",
            // },
            {
                title: "邀请有礼",
                path: "/pages/invitation/invitation",
                image: '9',
            },
            {
                title: "会员中心",
                path: "",
                image: '10',
            },
            // {
            //     title: "初见订单",
            //     path: "/pages/cha-order/cha-order",
            //     image: '10',
            // },
            // {
            //     title: "初见注册",
            //     path: "",
            //     image: '10',
            //     type: 'fn',
            //     name: 'register'
            // },
            // {
            //     title: "点餐记录",
            //     image: '10',
            //     path: "/pages/order-list/order-list",
            // },
            // {
            //     title: "点餐订单",
            //     path: "/pages/order-list/order-list",
            //     image: '10',
            // },
        ],
        user: {},
    },
    closeWifi() {
        this.setData({
            showWifi: false
        })
    },

    getWifiDesc() {
        let store = wx.getStorageSync('firststore')
        let targetStore
        if (!store) {
            return toast({title: '未选择店铺'})
        }
        if (Array.isArray(store)) {
            targetStore = store[0]
        } else if (typeof store === 'object') {
            targetStore = store
        }
        const {id: store_id} = targetStore
        app.ajax({
            url: '/api/room/storewifi',
            data: {store_id},
            success: res => {
                const {code, data, msg} = res
                if (code !== 1) return toast({title: msg})
                if (!data.length) return toast({title: '当前门店无wifi'})
                const [target] = data
                const {wifi_account, wifi_password} = target
                this.setData({
                    wifi: {
                        name: wifi_account,
                        password: wifi_password,
                    },
                    showWifi: true
                })
            }
        })
    },

    toUrl(ev) {
        const {path} = ev.currentTarget.dataset
        wx.navigateTo({
            url: path
        })
    },
    closePop() {
        this.setData({
            showMobile: false
        })
    },
    dyHand() {
        wx.navigateTo({
            url: '/pages/group-buying/group-buying?type=2'
        })
    },
    async onShow() {
        this.userinfo()
        const {couponLen, groupLen} = await app.initCouponData()
        this.setData({
            couponLen, groupLen
        })
    },
    toWallet() {
        wx.navigateTo({
            url: '/pages/wallet/wallet'
        })
    },
    userinfo: function () {
        app.ajax({
            url: '/api/user/userinfo',
            success: res => {
                if (res.code === 1) {
                    const {data} = res
                    wx.setStorageSync('user', data);
                    this.setData({user: data})
                    app.setUserData(data)
                }
            }
        })
    },

    clickMenuItem(ev) {
        const {path, type, name} = ev.currentTarget.dataset
        if (type && type === 'fn') {
            if (name === 'callPhone') {
                return this.callPhone()
            }
            if (name === 'mobile') {
                this.setData({
                    showMobile: true
                })
                return;
            }
            if (name === 'wifi') {
                return this.getWifiDesc()
            }
            if (name === 'register') {
                return wx.navigateToMiniProgram({
                    appId: 'wxc4c033f5c53f00c5',
                    path: '/pages/login/login', //路径和携带的参数
                    envVersion: 'release',
                    success(res) {
                        // 打开成功
                    },
                    fail(res) {
                        // 打开失败
                    },
                    complete(res) {
                        // 调用结束  不管成功还是失败都执行
                    }
                    /**
                     * appId：跳转到的小程序app-id
                     * path：打开的页面路径，如果为空则打开首页，path 中 ? 后面的部分会成为 query，在小程序的 App.onLaunch、App.onShow 和 Page.onLoad的回调函数中获取query数据
                     * extraData：需要传递给目标小程序的数据，目标小程序可在 App.onLaunch、App.onShow 中获取到这份数据
                     * envVersion：要打开的小程序版本，有效值: develop（开发版），trial（体验版），release（正式版），仅在当前小程序为开发版或体验版时此参数有效，如果当前小程序是正式版，则打开的小程序必定是正式版
                     */
                })
            }
        }
        if (!path) {
            toast({title: '暂未开发'})
            return
        }
        wx.navigateTo({
            url: path
        })
    },

    callPhone: function () {
        const tel = wx.getStorageSync('tel');
        if (tel)
            wx.makePhoneCall({
                phoneNumber: tel,
                success: function () {
                },
            })
    },

    my_wallet: function () {
        wx.navigateTo({
            url: '/pages/my_wallet/my_wallet',
        })
    },

    meituan: function () {
        wx.navigateTo({
            url: '/pages/meituan/meituan',
        })
    },
    coupon: function () {
        wx.navigateTo({
            url: '/pages/coupon/coupon',
        })
    },
    // 充值
    account_chongzhi: function () {
        wx.navigateTo({
            url: '/pages/account_chongzhi/account_chongzhi',
        })
    },

    updateUserInfo({type, value}) {
        if (type === 1) {
            return this.userinfo()
        }
        wx.setStorageSync('user', value);
        this.setData({
            user: value
        })
        app.setUserData(value)
    },

    onUnload() {
        eventBus.$off('updateUserInfo', this.updateUserInfo)
    },
    onLoad: async function (options) {

        eventBus.$on('updateUserInfo', this.updateUserInfo)
    }
});
