Component({
    properties: {
        visible: {
            type: Boolean,
        }
    },
    data: {
        mobile: '15967780090',
    },
    methods: {
        closeHand() {
            this.triggerEvent('close', false)
        },
        callPhone() {
            const {mobile} = this.data
            wx.makePhoneCall({
                phoneNumber: mobile,
                success: () => {

                },
                fail(res) {

                },
                complete: () => {
                    this.closeHand()
                }
            })
        }
    }
});
