const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        date: '2020-06'
    },
    bindDateChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            date: e.detail.value
        })
        this.getMoneyLog();
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var y = new Date().getFullYear(), m = new Date().getMonth() + 1;
        m = ('0' + m).slice(-2);
        this.setData({
            date: y + '-' + m
        });
        this.getMoneyLog();
    },

    getMoneyLog: function(){
        app.ajax({
            url: '/api/user/moneyLog',
            data: {
                date: this.data.date
            },
            success: res => {
                if(res.code == 1){
                    this.setData({
                        list: res.data
                    })
                }else{
                    this.setData({
                        list: []
                    })
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})