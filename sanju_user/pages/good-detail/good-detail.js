import {toast} from "../../utils/tool";
import EventBus from "../../utils/eventBus";

const app = getApp()
Page({
    data: {
        foodData: {},
    },
    addHandler() {
        EventBus.$emit('addGoodSuccess', this.data.foodData)
        toast({title: '添加成功'})
        wx.navigateBack()
    },
    initData(id) {
        app.ajax({
            url: "/api/food/foodgooddetail",
            data: {id},
            success: resp => {
                const {code, data, msg} = resp
                if (code !== 1) return toast({title: msg})
                this.setData({
                    foodData: {...data}
                })
            }
        })
    },
    onLoad: function (options) {
        if (options.hasOwnProperty('id')) {
            this.initData(options.id)
        }
    }
});