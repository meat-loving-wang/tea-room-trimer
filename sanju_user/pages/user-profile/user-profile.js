import {toast} from "../../utils/tool";
import EventBus from "../../utils/eventBus";

const app = getApp()
Page({
    data: {
        formData: {
            avatar: '',
            username: '',
            mobile: '',
        },
        userInfo: {}
    },
    updateData() {
        app.ajax({
            url: '/api/user/userinfo',
            success: res => {
                if (res.code === 1) {
                    const {data} = res
                    EventBus.$emit('updateUserInfo', {
                        type: 2,
                        value: data
                    })
                    const {formData} = this.data
                    const {mobile} = data
                    this.setData({
                        formData: {...formData, mobile}
                    })
                }
            }
        })
    },
    getPhoneNumber(ev) {
        app.ajax({
            url: '/api/user/loginwxa',
            data: {code: ev.detail.code},
            success: resp => {
                const {code, msg, data} = resp
                toast({title: msg})
                if (code === 1) {
                    this.updateData()
                }
            }
        })
    },
    onLoad: function () {
        const data = app.getUserData()
        this.userInfo = data
        const {nickname: username, avatar, mobile} = data
        this.setData({
            formData: {
                avatar, username,
                mobile
            }
        })
    },
    confirmHandler() {
        const {formData} = this.data
        const {username, avatar} = formData
        if (username.length === 0) return toast({title: '请输入用户名'})
        if (avatar.length === 0) return toast({title: '请上传头像'})
        app.ajax({
            url: '/api/user/updateuserinfo',
            data: {
                nickname: username, avatar
            },
            success: resp => {
                const {code, msg} = resp
                toast({title: msg})
                if (code === 1) {
                    EventBus.$emit('updateUserInfo', {
                        type: 1,
                        value: null
                    })
                }
            }
        })
    },
    onInput(ev) {
        console.log(ev, 'input')
        const {value} = ev.detail
        const {formData} = this.data
        this.setData({
            formData: {
                ...formData,
                username: value
            }
        })
    },
    uploadAvatar(filePath) {
        const token = wx.getStorageSync('token')
        wx.uploadFile({
            url: 'https://sanju.xinyunweb.com/api/newupload/postImage',
            filePath,
            name: 'file',
            timeout: 1000 * 10,
            header: {token},
            success: resp => {
                console.log(resp, 'uploadRes')
                const {statusCode, data} = resp
                if (statusCode !== 200) {
                    return toast({title: '图片上传失败'})
                }
                const result = JSON.parse(data)
                const {code, data: _data, msg} = result
                if (code !== 1) return toast({title: msg})
                const {formData} = this.data
                this.setData({
                    formData: {
                        ...formData,
                        avatar: _data,
                    }
                })
            },
            fail: () => {
                toast({title: '图片上传失败'})
            }
        })
    },
    choseAvatar(ev) {
        const {avatarUrl} = ev.detail
        if (!avatarUrl) return
        this.uploadAvatar(avatarUrl)
    },
});
