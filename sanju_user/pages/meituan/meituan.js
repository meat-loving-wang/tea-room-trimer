import {toast} from "../../utils/tool";

const app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
	    sessioncode:false,
		inputValue:'',
		// 接收的订单id
		orderId:'',
		// 接收的时长
		nums:''
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		this.setData({
			orderId:options.orderID,
			nums:options.num
		})
		// wx.request({
		//   url: 'http://gongxiangchashi.rchz.top/index.php/api/scanprepare/getAuthCode', //仅为示例，并非真实的接口地址
		//   method:'GET',
		//   success (res) {
		// 	  console.log(res);
		// 	// console.log(res.data)
		//   }
		// })
	},
	scanCode:function(){
		let _this = this;
		wx.scanCode({
		  success (res) {
		    console.log(res)
			_this.setData({
				inputValue:res.result
			})
		  }
		})
	},
	 getSearchValue(e) {
		 this.setData({
			 inputValue:e.detail.value
		 })
	 },
	commit:function(){
		const {inputValue} = this.data
		if (!inputValue) return toast({title:'请输入验券码'})
		let _this = this;
		app.ajax({
			url: '/api/scanprepare/consume',
			method:'GET',
			data: {
				qr_code: this.data.inputValue,
				nums:this.data.nums,
				orderId:this.data.orderId
			},
			success: res => {
				console.log(res);
				wx.showToast({
					title: res.msg,
					icon: 'none'
				});
				_this.setData({
					inputValue:''
				})
				if(res.code != 0){
					wx.showLoading({
						title:"正在加载..."
					})
					setTimeout(()=>{
						wx.hideLoading()
						wx.navigateTo({
							url:`/pages/pay_success/pay_success?order_id=${this.data.orderId}`
						})
					},1000)
				}
			}
		})
	}
})
