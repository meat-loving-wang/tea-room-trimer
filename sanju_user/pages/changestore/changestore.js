import {delay, toast} from "../../utils/tool";
import EventBus from "../../utils/eventBus";
import {BASE_URL} from "../../constant/index";

const app = getApp()
Page({
    data: {
        storeList: [],
        type: '',
        isScroll: false,
    },
    openDoor(store_id) {
        app.ajax({
            url: '/api/frontdoor/openfrontdoor',
            data: {},
            success: res => {
                wx.hideLoading()
                toast({title: res.msg})
            }
        })
    },
    findStoreByOrder() {
        wx.showLoading({title: '加载中...', mask: true})
        this.openDoor()
        return
        app.ajax({
            url: '/api/user/orderList',
            data: {status: '30', page: 1},
            success: async res => {
                await delay(200)
                if (!res.data) res.data = []
                if (!res.data.length) {
                    wx.hideLoading()
                    return toast({title: '您没有预约订单,不能开门'})
                }
                const [firstOrder] = res.data
                const {store_id} = firstOrder
                const targetStore = this.data.storeList.find(item => item.id === store_id)
                if (!targetStore) {
                    wx.hideLoading()
                    return toast({title: '没有相关店铺'})
                }

            }
        })
    },

    reorderHandle() {
        wx.navigateTo({
            url: '/pages/history_order/history_order'
        })
    },
    onShareAppMessage() {
        return {
            title: '博戏互娱',
            path: "/pages/store-list/store-list",
        }
    },
    scrollHandle() {
        if (this.timerId) {
            clearTimeout(this.timerId)
            this.timerId = null
        }
        this.setData({isScroll: true})
        this.timerId = setTimeout(() => {
            this.setData({isScroll: false})
        }, 300)
    },
    changestore(e) {
        const {store} = e.currentTarget.dataset
        const currentStore = app.globalData.firstStore
        if (store.id !== currentStore.id) app.clearOrderCache()
        app.globalData.firstStore = store
        const {type} = this.data
        EventBus.$emit('changeStore', store)
        toast({title: '选择成功'})
        setTimeout(() => {
            if (type === 'back') {
                return wx.navigateBack()
            }
            wx.switchTab({
                url: '/pages/index/index',
            })
        }, 2000)
    },
    previewImage(ev) {
        const {image} = ev.currentTarget.dataset
        if (!image) return
        wx.previewImage({urls: [image]})
    },
    // 打开地图
    openMap: function (e) {
        let mapdata = e.currentTarget.dataset.mapdata
        let arr = mapdata.lat.split(",")
        var lat = parseFloat(arr[1]),
            lon = parseFloat(arr[0]),
            firstStore = this.data.firstStore;
        if (!lat || !lon) {
            return false;
        }
        wx.openLocation({
            latitude: lon,
            longitude: lat,
            scale: 15,
            name: mapdata.name,
            address: mapdata.address,
            success: function (res) {
            },
            fail: function (res) {
            }
        })
    },
    getStore(e) {
        app.ajax({
            url: "/api/room/storelist",
            data: {
                lat: e.lat,
                lon: e.lon
            },
            success: res => {
                const {code, msg} = res
                if (!res.data) res.data = []
                if (code !== 1) return toast({title: msg})
                this.setData({
                    storeList: res.data.map(item => {
                        item.image = BASE_URL + item.image
                        return item
                    })
                })
            }
        })
    },
    callPhone(e) {
        wx.makePhoneCall({
            phoneNumber: e.currentTarget.dataset.phone,
            success: function () {
            },
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let location = wx.getStorageSync('location')
        this.getStore(location)
        if (options.hasOwnProperty('type')) {
            const {type} = options
            this.data.type = type
        }
    },
})
