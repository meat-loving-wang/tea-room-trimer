Component({
    properties: {
        visible: {
            type: Boolean,
        },
        list: {
            type: Array
        }
    },
    methods: {
        clearHandler() {
            wx.showModal({
                content: '确定删除所有物品吗?',
                success: res => {
                    if (res.cancel) return
                    this.triggerEvent('clear', true)
                }
            })
        },
        decrement(ev) {
            const {id, goods} = ev.currentTarget.dataset
            const {haveSpec, targetSpec} = goods
            const payload = {
                type: "decrement",
                id: id,
                spec: haveSpec,
                specId: targetSpec
            }
            this.triggerEvent('change', payload)
        },
        increment(ev) {
            const {id, goods} = ev.currentTarget.dataset
            const {haveSpec, targetSpec} = goods
            const payload = {
                type: "increment",
                id: id,
                spec: haveSpec,
                specId: targetSpec
            }
            this.triggerEvent('change', payload)
        },
        onClose() {
            this.triggerEvent('close', false)
        }
    }
});
