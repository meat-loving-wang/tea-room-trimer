import {BASE_URL, SPEC_DATA} from "../../../constant/index";

Component({
    properties: {},
    data: {
        baseUrl: BASE_URL,
        goodsData: {
            name: '',
            image: '',
            num: 1,
            count: '',
            price: '',
        },
        visible: false,
        index: -1,
        activeSpec: 0,
        activeSpecName: '',
        number: 1,
        specList: SPEC_DATA
    },
    methods: {
        confirmHandle() {
            const {
                activeSpec,
                activeSpecName,
                number,
                goodsData,
                index
            } = this.data
            this.triggerEvent('confirm', {
                goodsData,
                specId: activeSpec,
                specName: activeSpecName,
                number,
                index,
            })
            this.onClose()
        },

        updateSpecName(value) {
            const target = SPEC_DATA.find(item => item.value === value)
            if (!target) return
            this.setData({
                activeSpecName: target.label
            })
        },
        numberChange(ev) {
            const {value} = ev.currentTarget.dataset
            const {number} = this.data
            const v = value + number
            if (v <= 0) return
            this.setData({
                number: v
            })
        },
        changeSpec(ev) {
            const {value} = ev.currentTarget.dataset
            if (value === this.data.activeSpec) return
            this.setData({
                activeSpec: value
            })
            this.updateSpecName(value)
        },
        open(index, goods) {
            console.log(goods, 'goods')
            this.setData({
                index,
                visible: true,
                goodsData: goods,
            })
            this.updateSpecName(this.data.activeSpec)
        },
        onClose() {
            this.setData({
                visible: false,
            })
            setTimeout(()=>{
                this.setData({
                    index: -1,
                    activeSpec: 0,
                    activeSpecName: '',
                    number: 1,
                })
            },300)
        }
    }
});
