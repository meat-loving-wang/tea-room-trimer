import {toast} from "../../utils/tool";

const app = getApp();
Page({
    data: {
        order_id: 0,
        order: {},
        user: {},
        pay_type: 2,
        coupon_id: 0,
        coupon_sub: 0,
        vip_sub: 0,
        pay_price: 0
    },


    pay_success: function () {
        var disclaimer = wx.getStorageSync('disclaimer');
        var that = this
        if (this.data.pay_type == 3) {
            wx.navigateTo({
                url: `/pages/meituan/meituan?num=${that.data.order.num}&orderID=${that.data.order_id}`
            })
            return false;
        }
        wx.showModal({
            title: '免责声明',
            content: disclaimer,
            cancelText: "取消",
            confirmText: "同意", //默认是“确定”
            success: function (res) {
                if (res.confirm) { //这里是点击了确定以后
                    wx.requestSubscribeMessage({
                        'tmplIds': [
                            'LPRQriYCiz0M_8SC1B35MxXwfF9NPR15rTO5UK7-V98',
                            'ILerHD6MYqAyGnyJ_AeVFSlqEMguQ24cTaT59Xot4Sw',
                            'mCcWRiaJcHNjX4A2WBu-ugdeD08yVZo7rFUBc5UptEc'
                            // 'ILerHD6MYqAyGnyJ_AeVFYMsR1yOg5GyeqpqAY6LQUY'
                        ],
                        success() {


                        },
                        complete() {
                            app.ajax({
                                url: '/api/user/masterPay',
                                data: {
                                    order_id: that.data.order_id,
                                    // coupon_id: that.data.coupon_id,
                                    pay_type: that.data.pay_type
                                },
                                success: res => {
                                    if (res.code == 1) {
                                        if (res.data) {
                                            wx.requestPayment({
                                                nonceStr: res.data.nonceStr,
                                                package: res.data.package,
                                                paySign: res.data.paySign,
                                                timeStamp: res.data.timeStamp,
                                                signType: res.data.signType,
                                                success: () => {
                                                    wx.redirectTo({
                                                        url: '/pages/pay_success2/pay_success2'
                                                    })
                                                },
                                                fail: () => {
                                                    toast({title:'支付失败'})
                                                }
                                            })
                                        } else {
                                            // wx.redirectTo({
                                            //   url: '/pages/pay_success/pay_success?order_id=' + that.data.order_id
                                            // })
                                        }
                                    } else {
                                        wx.showToast({
                                            title: res.msg,
                                            icon: 'none'
                                        })
                                        wx.redirectTo({
                                            url: '/pages/pay_success2/pay_success2'
                                        })
                                    }
                                }
                            }, 1);
                        }
                    })
                } else { //这里是点击了取消以后
                    return false;
                }
            }
        })


    },
    account_chongzhi: function () {
        wx.navigateTo({
            url: '/pages/account_chongzhi/account_chongzhi',
        })
    },
    setPayType: function (e) {
        var type = e.currentTarget.dataset.type;
        this.setData({
            pay_type: type
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var order_id = options.order_id || 0,
            user = wx.getStorageSync('user');
        this.setData({
            order_id: order_id,
            user: user
        });

        this.getOrder();
    },

    getOrder: function () {
        app.ajax({
            url: '/api/user/getOrderMaster',
            data: {
                order_id: this.data.order_id
            },
            success: res => {
                if (res.code == 1) {
                    var coupon_id = 0,
                        coupon_sub = 0,
                        vip_sub = 0,
                        pay_price = 0,
                        yj_price = parseFloat(res.data.yj_price),
                        total_price = parseFloat(res.data.info.total_price);

                    // if (res.data.coupons.length > 0) {
                    //     coupon_sub = parseFloat(res.data.coupons[0].money);
                    //     coupon_id = res.data.coupons[0].id;
                    // }
                    pay_price = total_price - coupon_sub;

                    var rate = parseFloat(res.data.discount_rate);
                    // if (rate > 0) {
                    //     vip_sub = pay_price * (10 - rate) / 10;
                    //     vip_sub = vip_sub.toFixed(2);
                    //     vip_sub = parseFloat(vip_sub);
                    // }
                    pay_price = pay_price - vip_sub;
                    // pay_price = pay_price + yj_price;
                    pay_price = pay_price.toFixed(2);
                    this.setData({
                        order: res.data.info,
                        coupon_id: coupon_id,
                        coupon_sub: coupon_sub,
                        vip_sub: vip_sub,
                        pay_price: pay_price
                    })
                } else {
                    wx.showToast({
                        title: res.msg,
                        icon: 'none'
                    })
                }
            }
        }, 1);
    },


})
