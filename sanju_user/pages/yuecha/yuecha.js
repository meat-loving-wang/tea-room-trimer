// pages/yuecha/yuecha.js
const app = getApp();
Page({
  data: {
    list: [],
    id: '',
  
  },
  previewSqs(e) {
    let currentUrl = e.currentTarget.dataset.src;
    console.log(currentUrl)
    // 微信预览图片的方法
    wx.previewImage({
      current: currentUrl, // 图片的地址url
      urls: [currentUrl] // 预览的地址url
    })
  },
  yuyue(e) {
    console.log(e.currentTarget.dataset.id)
    this.setData({
      id: e.currentTarget.dataset.id
    })
    wx.navigateTo({
      url: '/pages/yuyue/yuyue?id=' + e.currentTarget.dataset.id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    app.ajax({
      url: '/api/user/teaMasterList',
      success: res => {
        this.setData({
          list: res.data.list.data,
        })
      }
    });
  },
})
