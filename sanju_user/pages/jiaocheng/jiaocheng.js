// pages/jiaocheng/jiaocheng.js
import {toast} from "../../utils/tool";
import {BASE_URL} from '../../constant/index'

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        jiaocheng: '',
        content: '',
        title: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    generateRichText(content) {


        // let noticeContent = '<div><img src="profile/upload/2021/11/22/tupian20210619144044.png"/></div>';

        let imgReg = RegExp(/<img [^>]*src=['"]([^'"]+)[^>]*>/gi);//定义正则，筛选出img元素
        let matchRes = content.match(imgReg);
        if (matchRes) {
            matchRes.forEach((item, index) => {
                let _tempStr = item.slice(0, item.length - 2);
                let _index = _tempStr.indexOf('/');
                let _str = _tempStr.substring(0, _index) + BASE_URL + _tempStr.substring(_index, _tempStr.length) + '"/>';
                content = content.replace(item, _str)
            })
        }
        return content
    },
    onLoad(options) {
        app.ajax({
            url: '/api/content/instruction',
            data: {
                type: 1
            },
            success: res => {
                const {code, data, msg} = res
                if (code !== 1) return toast({title: msg})
                const [targetData] = data
                const content = this.generateRichText(targetData.content)
                this.setData({
                    title: targetData.title,
                    content,

                    // jiaocheng: res.data[0]
                });
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})
