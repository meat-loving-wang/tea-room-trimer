import {STATUS_HEIGHT} from "../../constant/index";
import {backHandler, toast} from "../../utils/tool";

const app = getApp()
Page({
    data: {
        height: STATUS_HEIGHT + 'px',
        active: 0,
        payOption: [],
        couponList: [],
        groupList: [],
        event: '',
    },
    onChange(event) {
        this.setData({
            active: event.detail.index
        })
    },
    back() {
        backHandler()
    },
    swiperChange(ev) {
        const {current: active} = ev.detail
        this.setData({
            active
        })
    },
    initPayOption() {
        const  {firstStore} = app.globalData
        if(!firstStore){
            return toast({title:'未选择店铺'},()=>{
                wx.redirectTo({
                    url:'/pages/store-list/store-list'
                })
            })
        }
        app.ajax({
            url: '/api/recharge/lists',
            data:{store_id: firstStore.id},
            success: resp => {
                const {code, data, msg} = resp
                if (code !== 1) {
                    return toast({title: msg})
                }
                if (data && data.length) {
                    this.setData({
                        payOption: [...data]
                    })
                }
            }
        })
    },
    onLoad: async function (options) {
        this.initPayOption()
        if (options.hasOwnProperty('type')) {
            const active = options.type * 1
            if (active <= 3) {
                this.setData({
                    active
                })
            }
        }
        if (options.hasOwnProperty('event')) {
            const {event} = options
            this.setData({event})
        }
        const {coupon} = await app.initCouponData()
        // const {coupon, group} = await app.initCouponData()
        this.setData({
            couponList: coupon.map(item => {
                const createTime = app.formatDate(item['create_time'])
                return {
                    ...item,
                    createTime,
                }
            })
        })
    }
});
