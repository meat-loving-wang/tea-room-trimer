import {toast} from "../../../utils/tool";
import EventBus from "../../../utils/eventBus";

const app = getApp()
Component({
    properties: {
        options: {
            type: Array,
            value: []
        },
        event: {
            type: String,
            value: '',
        }
    },
    data: {
        checked: false,
        activeIndex: 0,
        money: '0.00'
    },

    pageLifetimes: {
        show() {
            const {globalData: {userInfo}} = app
            if (!!userInfo) {
                const {money} = userInfo
                this.setData({
                    money
                })
            }
        }
    },

    methods: {
        confirmHandler() {
            const store = app.globalData.firstStore
            if (!store || !store.hasOwnProperty('id')) {
                return toast({title: '未选择店铺'})
            }
            const {id: store_id, name: storeName} = app.globalData.firstStore
            const {checked, activeIndex, options} = this.data
            if (!checked) {
                return toast({title: '请同意用户协议'})
            }
            wx.showModal({
                title: '提示',
                content: `您确定充值到${storeName}吗?`,
                success: ev => {
                    if (ev.cancel) return
                    const targetMeal = options[activeIndex]
                    const {id} = targetMeal
                    this.recharge(id, store_id)
                }
            })
        },

        lookDetail() {
            wx.navigateTo({
                url: '/pages/xiaofei_mingxi/xiaofei_mingxi',
            })
        },

        recharge: function (recharge_id,store_id) {
            const {event} = this.data
            app.ajax({
                url: '/api/user/recharge',
                data: {recharge_id,store_id},
                success: res => {
                    const {msg, code} = res
                    if (code === 1) {
                        wx.requestPayment({
                            nonceStr: res.data.nonceStr,
                            package: res.data.package,
                            paySign: res.data.paySign,
                            timeStamp: res.data.timeStamp,
                            signType: res.data.signType,
                            success: () => {
                                toast({title: '支付成功'})
                                app.updateUserInfo().then(data => {
                                    if (!!data) {
                                        const {money} = data
                                        this.setData({money})
                                    }
                                    if (!!event) {
                                        EventBus.$emit('accountSuccess', data)
                                    }
                                })
                                app.initCouponData();

                            }
                        })
                        return
                    }
                    toast({title: msg})
                }
            }, 1);
        },

        choseMeal(ev) {
            const {index} = ev.currentTarget.dataset
            const {activeIndex} = this.data
            const value = index * 1
            if (value === activeIndex) return
            this.setData({
                activeIndex: value
            })
        },

        getRecharge() {
            app.ajax({
                url: '/api/index/chargeContent',
                success: res => {
                    if (res.code === 1) {
                        this.setData({
                            recharge: res.data
                        })
                    } else {
                        toast({title: res.msg})
                    }
                }
            }, 1);
        },

        onChange(ev) {
            const {detail: checked} = ev
            this.setData({
                checked
            })
        }
    }
});
