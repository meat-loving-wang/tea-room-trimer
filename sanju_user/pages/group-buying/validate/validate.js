import {toast} from "../../../utils/tool";

Component({
    properties: {
        orderId: {
            type: String
        },
        nums: {
            type: String,
        }
    },
    data: {
        inputValue: '',
        // 接收的订单id
    },
    methods: {
        getSearchValue(e) {
            this.setData({
                inputValue: e.detail.value
            })
        },
        commit() {
            const {inputValue, nums, orderId} = this.data
            // console.log(nums, orderId)
            // return
            if (!inputValue.length) return toast({title: "请输入验券码"})
            app.ajax({
                url: '/api/scanprepare/consume',
                method: 'GET',
                data: {qr_code: inputValue, nums, orderId,},
                success: res => {
                    const {msg, code} = res
                    toast({title: msg})
                    if (code === 1) {
                        this.setData({
                            inputValue: ''
                        })
                        wx.showLoading({
                            title: "正在加载..."
                        })
                        setTimeout(() => {
                            wx.hideLoading()
                            wx.navigateTo({
                                url: `/pages/pay_success/pay_success?order_id=${orderId}`
                            })
                        }, 1000)
                    }
                }
            })
        },
        scanCode: function () {
            wx.scanCode({
                success: res => {
                    console.log(res)
                    this.setData({
                        inputValue: res.result
                    })
                }
            })
        },
    }
});
