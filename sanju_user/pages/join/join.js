const app = getApp();
Page({
    data: {
        city: '',
        name: '',
        mobile: '',
        msg: ''
    },
    company_intr: function () {
        wx.navigateTo({
            url: '/pages/company_intr/company_intr'
        })
    },
    callPhone: function() {
        const tel = wx.getStorageSync('tel');
        if(!tel) return
        wx.makePhoneCall({
            phoneNumber: tel,
        })
    },

    setCity: function(e){
        this.setData({
            city: e.detail.value
        })
    },

    setName: function(e){
        this.setData({
            name: e.detail.value
        })
    },

    setMobile: function(e){
        this.setData({
            mobile: e.detail.value
        })
    },

    setMsg: function(e){
        this.setData({
            msg: e.detail.value
        })
    },

    save: function(){
        var city = this.data.city, name = this.data.name, mobile = this.data.mobile, msg = this.data.msg;
        if(!city || !name || !mobile) {
            wx.showToast({
              title: '请填入表单内容',
              icon: 'none'
            });
            return false;
        }
        app.ajax({
            url: '/api/user/join',
            data: {
                city: city,
                name: name,
                mobile: mobile,
                msg: msg
            },
            success: res => {
                wx.showToast({
                  title: res.msg,
                  icon: 'none'
                });
                if(res.code == 1){
                    setTimeout(function(){
                        wx.navigateBack({
                            delta: 1,
                          })
                    }, 1500);
                }
            }
        }, 1);
    },
})