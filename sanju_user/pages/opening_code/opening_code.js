import {toast} from "../../utils/tool";

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        seeDetails: true,
        order_id: 0,
        order: {},
        type: 0,
        isshow: true,
        reorderList: [],
    },
    see_details: function () {
        var seeDetails = this.data.seeDetails
        seeDetails = !seeDetails
        this.setData({
            seeDetails: seeDetails
        })
    },

    code: function () {
        wx.navigateTo({
            url: '/pages/pay_success/pay_success?order_id=' + this.data.order_id
        })
    },
    formatTime(timestamp) {
        const date = new Date(timestamp)
        const [year, month, day, hour, minute, seconds] = [date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()]

        function patch(value) {
            return ('0' + value).slice(-2)
        }

        return `${year}-${patch(month)}-${patch(day)} ${patch(hour)}:${patch(minute)}:${patch(seconds)}`
    },
    getReorderData(order_id) {
        app.ajax({
            url: "/api/user/orderdetailxudan",
            data: {order_id},
            success: resp => {
                const {data} = resp
                if (data.length) {
                    data.reverse()
                    this.setData({
                        reorderList: data.map(item => {
                            const {create_time, num} = item
                            item.create_time = this.formatTime(create_time * 1000)
                            item.num = num.toFixed(1)
                            return item
                        })
                    })
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    async onLoad(options) {
        const loginRes = await app.getUserInfo()
        const {id: userId} = loginRes.data
        let self = this;
        const {order_id} = options
        this.setData({order_id})
        this.getList(order_id)
        this.getReorderData(order_id)
        if (options.type == 1) {
            const {id} = options
            this.setData({
                type: options.type
            })
            console.log(id * 1 === userId * 1)
            if (id * 1 === userId * 1) {
                return
            }
            wx.showModal({
                title: '是否接收',
                success(res) {
                    if (res.cancel) return
                    const hasMobile = app.checkHasMobile()
                    if (!hasMobile) return;
                    self.setData({
                        isshow: false
                    })
                    // self.getList(this.data.order_id);
                    app.ajax({
                        url: '/api/user/transferOrder',
                        data: {
                            order_id: self.data.order_id
                        },
                        success: res => {
                            const {msg} = res
                            toast({title: '接收成功'}, () => {
                                wx.switchTab({
                                    url: '/pages/my-order/my-order'
                                })
                            })
                        }
                    }, 1);
                }
            })
        }
    },
    onShareAppMessage(res) {
        return {
            title: '亲 赠送给你一个茶室包间请 前去消费',
            path: `/pages/opening_code/opening_code?type=1&order_id=${this.data.order_id}`, //这里是被分享的人点击进来之后的页面
            imageUrl: '' //这里是图片的路径
        }
    },
    getOrderInfo() {

    },
    generateTime(data) {
        const {start_time, end_time} = data
        const d1 = new Date(start_time.replace(/-/g, '/'))
        const d2 = new Date(end_time.replace(/-/g, '/'))
        const timestamp = d2.getTime() - d1.getTime()
        const currentHour = (timestamp / (1000 * 60 * 60)).toFixed(1)
        return {...data, hours: currentHour}
    },
    getList: function (order_id) {
        app.ajax({
            url: '/api/user/orderDetail',
            data: {order_id},
            success: res => {
                if (res.code === 1) {
                    this.setData({order_id, order: this.generateTime(res.data)})
                } else {
                    wx.showToast({
                        title: res.msg,
                        icon: 'none'
                    })
                }
            }
        }, 1);
    },
})
