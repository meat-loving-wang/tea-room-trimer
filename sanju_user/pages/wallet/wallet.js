import {toast} from "../../utils/tool";

const app = getApp()
Page({
    data: {
        money: 0,
        payOption: []
    },
    initPayOption() {
        const {firstStore} = app.globalData
        if (!firstStore) {
            return toast({title: '未选择店铺'}, () => {
                wx.redirectTo({
                    url: '/pages/store-list/store-list'
                })
            })
        }
        app.ajax({
            url: '/api/recharge/lists',
            data: {store_id: firstStore.id},
            success: resp => {
                const {code, data, msg} = resp
                if (code !== 1) {
                    return toast({title: msg})
                }
                if (data && data.length) {
                    this.setData({
                        payOption: [...data]
                    })
                }
            }
        })
    },
    onLoad: function (options) {
        this.initPayOption()
        const {globalData: {userInfo}} = app
        if (!!userInfo) {
            const {money} = userInfo
            this.setData({
                money,
            })
        }
    }
});
