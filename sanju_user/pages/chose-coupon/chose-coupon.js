const app = getApp()
let eventChannel = null
Page({
    data: {
        price: 0,
        list: [],
    },
    confirmHandler() {
        const {list} = this.data
        const target = list.find(item => item.active)
        eventChannel.emit("choseCoupon", target)
        wx.navigateBack()
    },
    onUnload() {
        eventChannel.off('initData', this.initData)
        eventChannel = null
    },
    onLoad: function () {
        eventChannel = this.getOpenerEventChannel()
        eventChannel.on('initData', this.initData)
    },
    onChange(ev) {
        const {index} = ev.currentTarget.dataset
        const {detail} = ev
        const {list} = this.data
        list.forEach(item => {
            item.active = false
        })
        list[index].active = detail
        this.setData({
            list: [...list]
        })
    },
    initData({list, price, active}) {
        console.log(list, 'list')
        price *= 1
        this.setData({
            list: list.map(item => {
                let activeVal = active && active.id === item.id
                return {
                    ...item,
                    disabled: price < item.min,
                    active: activeVal,
                }
            }), price: price * 1
        })
    },
});