// pages/my_kabao/my_kabao.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {}
  },
  xiaofei_mingxi:function(){
    wx.navigateTo({
      url: '/pages/xiaofei_mingxi/xiaofei_mingxi',
    })
  },
  account_chongzhi: function () {
    wx.navigateTo({
      url: '/pages/account_chongzhi/account_chongzhi',
    })
  }, 
  apply_invoice:function(){
    wx.showToast({
      title: '如需申请发票，请联系门店负责人开具',
      icon: 'none',
      duration: 2000
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var user = wx.getStorageSync('user') || {};
    this.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})