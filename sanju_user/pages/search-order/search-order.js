import {toast, cloneDataFunc} from "../../utils/tool";
import {NAME_ENUM} from "../../constant/index";

const app = getApp();
let eventChannel = null
Page({
    data: {
        list: [],
        allData: [],
        keyword: '面包',
        currentGoodList: []
    },
    calcSpecGoodNumber(number, id) {
        const {currentGoodList} = this.data
        const target = currentGoodList.filter(item => item.id === id)
        console.log(target, '购物车target')
        if (!target || !target.length) return number
        return number + target.reduce((pre, cur) => {
            return pre + cur.specNumber
        }, 0)
    },
    // 保存购物车 更新allData的num 更新list中的num
    chooseSpecSuccess(ev) {
        const {currentGoodList, list, allData} = this.data
        const {number, specId, specName, goodsData} = ev.detail
        const {id} = goodsData
        const goodsDataTarget = allData.find(item => item.id === id)
        if (!goodsDataTarget || !goodsDataTarget.haveSpec) return;
        goodsDataTarget.num = this.calcSpecGoodNumber(number, id)
        // 查找 当前购物车中是否存在 当前商品选择的当前 spec
        const cartTarget = currentGoodList.find(item => item.id === id && item.haveSpec && item.targetSpec === specId)
        const cloneData = cloneDataFunc(goodsDataTarget)
        const listTarget = list.find(item => item.id === id)
        listTarget.num = goodsDataTarget.num
        this.setData({list})
        if (!cartTarget) {
            cloneData.targetSpec = specId
            cloneData.targetSpecName = specName
            cloneData.specNumber = number
            cloneData.specTotal = (number * cloneData.price).toFixed(2)
            currentGoodList.push(cloneData)
            wx.setStorageSync(NAME_ENUM, currentGoodList)
        } else {
            cartTarget.specNumber += number
            cartTarget.specTotal = (cartTarget.specNumber * cartTarget.price).toFixed(2)
            wx.setStorageSync(NAME_ENUM, currentGoodList)
        }
    },
    onInput(ev) {
        const {value} = ev.detail
        this.setData({
            keyword: value
        })
    },
    deleteCart(ev) {
        // eventChannel.emit('delHandler', ev)
        const {good} = ev.currentTarget.dataset
        const {id} = good
        const {list, currentGoodList} = this.data
        const targetGood = list.find(item => item.id === id)
        const num = targetGood.num - 1
        if (num <= 0) {
            targetGood.num = 0
        } else {
            targetGood.num = num
        }
        this.setData({
            list: [...list]
        })
        const _index = this.data.allData.findIndex(item => item.id === id)
        this.data.allData[_index].num = targetGood.num
        const t = currentGoodList.find(item => item.id === id)
        if (t) {
            t.num = targetGood.num
            wx.setStorageSync(NAME_ENUM, currentGoodList)
        }

    },
    chooseSpec(ev) {
        const {index, good} = ev.currentTarget.dataset
        const target = this.selectComponent('#specPopup')
        target && target.open(index, good)
    },
    addCart(ev) {
        const {good} = ev.currentTarget.dataset
        const {id} = good
        const {list, currentGoodList} = this.data
        const targetGood = list.find(item => item.id === id)
        targetGood.num += 1
        this.setData({list: [...list]})
        const _index = this.data.allData.findIndex(item => item.id === id)
        this.data.allData[_index].num = targetGood.num
        const t = currentGoodList.find(item => item.id === id)
        if (t) {
            t.num = targetGood.num
        } else {
            currentGoodList.push(targetGood)
        }
        wx.setStorageSync(NAME_ENUM, currentGoodList)

    },

    searchHandler() {
        const {keyword} = this.data
        if (!keyword.length) return toast({title: '请输入关键字'})
        const {allData} = this.data
        const list = allData.filter(item => {
            return item.name === keyword || item.name.includes(keyword)
        })
        this.setData({list})
    },

    getCartData([ev, allData]) {
        console.log(allData, 'allData')
        // if (!!ev && ev.length) {
        //     ev.forEach(item => {
        //         const index = allData.findIndex(good => good.id === item.id)
        //         if (index === -1) return
        //         const {index: _index} = allData[index]
        //         allData[index] = {...item, index: _index}
        //     })
        // }
        this.data.allData = allData
        this.setData({
            currentGoodList: wx.getStorageSync(NAME_ENUM) || [],
        })
        // this.data.allData = allData
    },
    calcNum(ev) {
        console.log(ev)
    },
    onLoad: function (options) {
        if (!eventChannel) {
            eventChannel = this.getOpenerEventChannel()
            eventChannel.on('getCartData', this.getCartData)
            eventChannel.on('calcNum', this.calcNum)
        }
    },
    onUnload() {
        if (!eventChannel) return
        eventChannel?.off('getCartData', this.getCartData)
        eventChannel?.off('calcNum', this.calcNum)
        eventChannel = null
    },

});
