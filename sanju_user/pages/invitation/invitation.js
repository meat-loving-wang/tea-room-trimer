import {toast} from "../../utils/tool";

const app = getApp()

Page({
    data: {
        openID: '',
        current: 0,
        list: [],
    },
    onShow() {
        const userData = app.getUserData()
        console.log(userData, 'useData')
        if (!!userData) {
            const {id} = userData
            this.data.openID = id
        } else {
            toast({
                title: '请您先登录',
            }, () => {
                wx.navigateBack()
            })
        }
    },
    swiperChange(ev) {
        this.setData({
            current: ev.detail.current
        })
    },
    choseTab(ev) {
        const {index} = ev.currentTarget.dataset
        if (this.data.current === index) return
        this.setData({
            current: index
        })
    },
    onShareAppMessage() {
        const {openID} = this.data
        const appName = '博戏互娱'
        // const path = '/pages/consumer/consumer?type=share&id=' + openID
        const path = '/pages/index/index?type=share&id=' + openID
        const promise = new Promise(resolve => {
            setTimeout(() => {
                resolve({
                    title: appName,
                    path,
                })
            }, 1000)
        })
        return {
            title: appName,
            path,
            promise
        }
    },

    initData() {
        app.ajax({
            url: "/api/user/myyaoqinguser",
            success: resp => {
                const {code, data, msg} = resp
                if (code !== 1) return toast({title: msg})
                console.log(resp,'resp')
                this.setData({
                    list: data.map(item => {
                        const {register_time} = item
                        return {
                            ...item,
                            register_time: app.formatDate(register_time)
                        }
                    }),
                })
            }
        })
    },

    onLoad: function (options) {
        this.initData()
    }
});