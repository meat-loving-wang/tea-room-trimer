const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        seeDetails: true,
        order_id: 0,
        order: {},
        type: 0,
        isshow: true
    },
    see_details: function () {
        var seeDetails = this.data.seeDetails
        seeDetails = !seeDetails
        this.setData({
            seeDetails: seeDetails
        })
    },

    code: function () {
        wx.navigateTo({
            url: '/pages/pay_success/pay_success?order_id=' + this.data.order_id
        })
    },
    goOrderList() {
        wx.navigateTo({
            url: '/pages/my_order/my_order'
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let self = this;
        var order_id = options.order_id

        this.setData({
            order_id: options.order_id
        })
        this.getList(options.order_id)
        if (options.type == 1) {
            this.setData({
                type: options.type
            })
            wx.showModal({
                title: '是否接收',
                success(res) {
                    if (res.confirm) {
                        self.setData({
                            isshow: false
                        })
                        // self.getList(this.data.order_id);
                        app.ajax({
                            url: '/api/user/transferOrder',
                            data: {
                                order_id: self.data.order_id
                            },
                            success: res => {
                                console.log(res, "接口返回");
                            }
                        }, 1);
                    } else if (res.cancel) {
                        console.log('用户点击取消')
                    }
                }
            })
        }
    },
    onShareAppMessage(res) {
        return {
            title: '亲 赠送给你一个茶室包间请 前去消费',
            path: `/pages/opening_code/opening_code?type=1&order_id=${this.data.order_id}`, //这里是被分享的人点击进来之后的页面
            imageUrl: '' //这里是图片的路径
        }
    },
    getOrderInfo() {

    },
    getList: function (order_id) {
        const format = time => {
            const date = new Date(time * 1000)
            const [year, month, day, hour, minute,seconds] = [date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(),date.getSeconds()]
			return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day} ${hour < 10 ? '0' + hour : hour}:${minute < 10 ? '0' + minute : minute}:${seconds < 10 ? '0' + seconds : seconds}`
        }
        app.ajax({
            url: '/api/user/getOrderMaster',
            data: {order_id},
            success: res => {
                const {info} = res.data
                const {starttime, endtime} = res.data.info
                info.starttime = format(starttime.split(' ')[1])
                info.endtime = format(endtime.split(' ')[1])
                if (res.code == 1) {
                    this.setData({
                        order_id: order_id,
                        order: res.data.info
                    })
                } else {
                    wx.showToast({
                        title: res.msg,
                        icon: 'none'
                    })
                }
            }
        }, 1);
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
})
