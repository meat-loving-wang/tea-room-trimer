const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
      list: [],
      is_ajax: false,
      is_over: false,
      status: 31,
      selectTime: false,
      clientHeight:0,
      room: {},
      store: {}, 
      id:0,
      order_id:0,
      times: {},
      allTimes:{},
      selected: [],
      select: [],
      
      select_date: '',
      select_start: '',
      select_end: '',
      nums: 0,

      date_arr: [],
      date_start: '',
      date_end: '',
      date: '',
  },

  select_time: function (e) {
    var id = e.currentTarget.dataset.id;
    var end = e.currentTarget.dataset.end;
	let room_id = e.target.dataset.roomid
    var order_id = e.currentTarget.dataset.order;
    var stores = wx.getStorageSync('stores'),store, room;
	
	wx.navigateTo({
	  url: `/pages/xudan/xudan?room_id=${room_id}&order_id=${order_id}`
	})
	
      //   for (var i in stores) {
      //       for (var j in stores[i].rooms) {
      //           if (stores[i].rooms[j].id == id) {
      //               room = stores[i].rooms[j];
      //               store = stores[i];
      //           }
      //       }
      //   }
      //   if (!store || !room) {
      //       wx.showToast({
      //           title: '查看内容不存在',
      //           icon: 'none'
      //       });
      //       setTimeout(function () {
      //           wx.navigateBack({
      //               delta: 1
      //           })
      //       }, 1500);
      //   }
      //   this.setData({
      //       id: id,
      //       room: room,
      //       store: store,
      //       select_start:end,
      //       order_id:order_id
      //   });

        
      // this.getSelectedTimes();
      // this.setData({
      //     selectTime: true
      // });
  }, 
  stopBubb: function () {
    return false
  },
  selectTime: function (e) {
    var times = this.data.allTimes,
        index = parseInt(e.currentTarget.dataset.index),
        time = e.currentTarget.dataset.time,
        select_start = this.data.select_start,
        select_end = this.data.select_end;
    
    select_end = index
    var error = false,nums = 0;
    for (var i in times) {
      times[i].select = false;
      if (i >= select_start && i <= select_end) {
          if (i!= select_start && times[i].disabled) {
              error = true;
          }
          times[i].select = true;
          nums += 0.5;
      }
    }
    if (nums > 0) nums -= 0.5;
    if (error) {
        wx.showToast({
            title: '存在不可选项',
            icon: 'none'
        });
        return false;
    }
    
    this.setData({
      select_end: select_end,
      allTimes: times,
      nums: nums
    })
    this.getTimes()
  },

   // 点击tab切换
   swichNav: function (e) {
      var date = e.currentTarget.dataset.date;
      if (this.data.select_date == date) {
          return false;
      } else {
          this.setData({
              select_date: date
          });
          this.getTimes();
      }
  },
  bindDateChange: function (e) {
      var date = e.detail.value;
      this.setData({
          date: date,
          select_date: date
      })
      this.getTimes();
  },

  getSelectedTimes: function () {
      app.ajax({
          url: '/api/index/getAllTImes',
          data: {
              id: this.data.id,
              date: this.data.select_date
          },
          success: res => {
              if (res.code == 1) {
                for (var i in res.data) {
                    if (i == this.data.select_start) {
                        res.data[i].select = true;
                    }
                  }
                  this.setData({
                      allTimes:res.data
                  })
                  this.getTimes()
              }
          }
      }, 1);
  },

  getTimes: function () {

      var datenow = this.data.select_date;
      datenow = datenow.replace(/-/g,"/")
      var start = new Date(datenow+' 00:00:00').getTime().toString()
      start = start.substr(0,10)
      start = parseInt(start)
      var end = start + 60*60*24 -1
      var times = {}
      for(let key  in this.data.allTimes){
          if(key>= start && key<=end){
              times[key] = this.data.allTimes[key] 
          }
      }
      this.setData({
          times:times
      })
  },

  close_time: function () {
      this.setData({
          selectTime: false
      });
  },
  evaluate:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/evaluate/evaluate?order_id=' + id,
    })
  },
  unsettled: function () {
    var nums = this.data.nums;
    if (nums *60<30) {
        wx.showToast({
            title: nums ? '预定时间'+this.data.room.min_time+'分钟起' : '请选择预定时间',
            icon: 'none'
        });
        return false;
    }
    var room_id = this.data.id,
    select_start = this.data.select_start,
        date = this.data.select_date,
        timeslot = [],
        times = this.data.allTimes
    for (var i in times) {
      if (times[i].select) {
            timeslot.push(times[i].key);
        }
    }
    app.ajax({
        url: '/api/user/addOrderNew',
        data: {
            room_id: room_id,
            is_renewal: 1,
            date: date,
            timeslot: timeslot.join(','),
            nums: nums
        },
        success: res => {
            if (res.code == 1) {
                wx.navigateTo({
                    url: '/pages/unsettled/unsettled?order_id=' + res.data.id
                })
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                });
            }
        }
    }, 1);
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
        wx.getSystemInfo({
            success: function (res) {
                console.log(res)
                that.setData({
                    clientHeight: res.windowHeight - 176
                });
            }
        })

    var date_arr = [];
    for (var i = 0; i < 3; i++) {
        var now = new Date().getTime() + i * 86400000;
        date_arr.push(this.getDate(now));
    }
    var start_date = this.getDate(Date.now() + 86400000 * 3),
        end_date = this.getDate(Date.now() + 86400000 * 7);
    this.setData({
        date_arr: date_arr,
        date_start: start_date.date,
        date_end: end_date.date,
        select_date: date_arr[0].date
    });
    this.getList();
  },

  getDate: function (now) {
          var date = new Date(now),
          y = date.getFullYear(),
          m = date.getMonth() + 1,
          w = date.getDay(),
          d = date.getDate(),
          weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
		  m = ('0' + m).slice(-2);
		  d = ('0' + d).slice(-2);
		  return {
			  week: weeks[w],
			  date: y + '-' + m + '-' + d,
			  name: m + '月' + d + '日'
		  };
  },

  getList: function(reset){
    if(reset){
      this.setData({
        page: 1,
        list: [],
        is_ajax: false,
        is_over: false
      });
    }
    if(this.data.is_ajax || this.data.is_over) return false;
    this.setData({
      is_ajax: true
    });
    var page = this.data.page, status = this.data.status, list = this.data.list, is_ajax = true, is_over = false;
    app.ajax({
      url: '/api/user/orderList',
      data: {
        status: status,
        page: page
      },
      success: res => {
        if(res.code == 1){
          page++;
          list = list.concat(res.data);
          var now = parseInt(Date.now() / 1000);
          for(var i in list){
            if(list[i].status == 10){
              list[i].gopay_time = parseInt((list[i].create_time + 1800 - now) / 60);
              if(list[i].gopay_time < 0) list[i].gopay_time = 0;
            }
          }
        }else{
          is_over = true;
        }
        is_ajax = false;
        this.setData({
          page: page,
          list: list,
          is_ajax: is_ajax,
          is_over: is_over
        });
      }
    })
  },

  
})