import {toast} from "../../../utils/tool";

const app = getApp()
Component({
    properties: {
        visible: {
            type: Boolean,
            value: false,
        },
        shareId: {
            type: String,
        }
    },
    data: {},
    methods: {
        confirmHandler() {
            const {shareId} = this.data
            this.triggerEvent('confirm', false)
            app.ajax({
                url: "/api/user/yaoqinguser",
                data: {yaoqing_user_id: shareId},
                success: resp => {
                    const {code, data, msg} = resp
                    if (code !== 1) {
                        // toast({title: msg})
                    }
                    toast({title: msg})
                    // this.triggerEvent('confirm',data)
                }
            })
        },
    }
});
