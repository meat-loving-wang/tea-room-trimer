Component({
    properties: {
        showPop: {
            type: Boolean,
        }
    },
    data: {},
    methods: {
        getSetting() {
            wx.openSetting({
                success: response => {
                    const {authSetting} = response
                    this.triggerEvent('setting', authSetting['scope.userLocation'])
                }
            })
        },
        confirmHandler() {

        }
    }
});
