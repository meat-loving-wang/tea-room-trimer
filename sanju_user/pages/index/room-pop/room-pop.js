Component({
    properties: {
        visible: {
            type: Boolean,
            value: false,
        }
    },
    data: {},
    methods: {
        closeHandler(){
          this.triggerEvent('closePop',false)
        },
    }
});
