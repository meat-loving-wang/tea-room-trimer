@keyframes scale {
  0% {
    transform: scale(1);
  }
  25% {
    transform: scale(1.08);
  }
  50% {
    transform: scale(1);
  }
  75% {
    transform: scale(1.08);
  }
}
.index-container {
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background: #F6F6F6;
  display: flex;
  flex-flow: column;
}
.index-container .room-popup-container {
  width: 100%;
  border-radius: 20rpx 20rpx 0 0;
  box-sizing: border-box;
  position: relative;
  background: #F6F6F6;
}
.index-container .room-popup-container .footer-wrap {
  margin-top: 40rpx;
  padding: 0 32rpx;
}
.index-container .room-popup-container .footer-wrap .footer {
  width: 100%;
  background: #FFFFFF;
  box-shadow: 0 8rpx 22rpx 0 rgba(188, 218, 190, 0.25);
  border-radius: 20rpx 20rpx 0 0;
  display: flex;
  align-items: center;
  padding: 24rpx 32rpx 14rpx 48rpx;
  box-sizing: border-box;
  justify-content: space-between;
}
.index-container .room-popup-container .footer-wrap .footer .right {
  display: flex;
}
.index-container .room-popup-container .footer-wrap .footer .right .btn-item {
  padding: 6rpx 28rpx 4rpx;
  border-radius: 20rpx 20rpx 0 0;
  opacity: 1;
  border: 2rpx solid #C5996A;
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  --color: #C5996A;
  --bg: #FFFFFF;
  background-color: var(--bg);
}
.index-container .room-popup-container .footer-wrap .footer .right .btn-item.active {
  --color: #FFFFFF;
  --bg: #C5996A;
  margin-left: 16rpx;
}
.index-container .room-popup-container .footer-wrap .footer .right .btn-item .title-1 {
  font-size: 28rpx;
  font-weight: bold;
  color: var(--color);
  line-height: 33rpx;
}
.index-container .room-popup-container .footer-wrap .footer .right .btn-item .title-2 {
  font-size: 24rpx;
  font-weight: bold;
  color: var(--color);
  line-height: 28rpx;
}
.index-container .room-popup-container .footer-wrap .footer .left {
  display: flex;
  flex-flow: column;
  align-items: flex-start;
}
.index-container .room-popup-container .footer-wrap .footer .left .tip {
  height: 30rpx;
  font-size: 22rpx;
  font-weight: bold;
  color: #8C8C8C;
}
.index-container .room-popup-container .footer-wrap .footer .left .check {
  display: flex;
  align-items: center;
}
.index-container .room-popup-container .footer-wrap .footer .left .check .label {
  height: 40rpx;
  font-size: 28rpx;
  font-weight: bold;
  color: #333333;
  margin-right: 4rpx;
}
.index-container .room-popup-container .footer-wrap .footer .left .check .value {
  height: 56rpx;
  font-size: 40rpx;
  font-weight: bold;
  color: #000000;
}
.index-container .room-popup-container .body-wrapper {
  margin-top: 32rpx;
  padding: 0 32rpx;
  box-sizing: border-box;
}
.index-container .room-popup-container .body-wrapper .list-wrap {
  box-sizing: border-box;
  width: 100%;
  height: 560rpx;
}
.index-container .room-popup-container .body-wrapper .list-wrap .swiper-box {
  width: 100%;
  height: 100%;
}
.index-container .room-popup-container .body-wrapper .list-wrap .list {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 22rpx;
}
.index-container .room-popup-container .body-wrapper .list-wrap .list_li {
  text-align: center;
  width: 100%;
  box-sizing: border-box;
  background: #FFFFFF;
  border-radius: 12rpx;
  font-size: 28rpx;
  font-weight: bold;
  color: #999999;
}
.index-container .room-popup-container .body-wrapper .list-wrap .list_li.active {
  background: #F2EDDC;
  color: #C5996A;
}
.index-container .room-popup-container .body-wrapper .list-wrap .list_li.disable {
  background: #ccc;
  color: #fff;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item {
  width: 100%;
  height: 136rpx;
  border-radius: 20rpx 20rpx 0 0;
  box-sizing: border-box;
  --color: #999999;
  --bg-color: #FFFFFF;
  background: var(--bg-color);
  display: flex;
  flex-flow: column;
  justify-content: center;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item.status-1 {
  --bg-color: #C5996A;
  --color: #ffffff;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item.status-2 {
  --color: #E9A81F;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item.status-3 {
  --color: #C5996A;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item.status-4 {
  --color: #FFFFFF;
  --bg-color: #C5996A;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item.status-5 {
  --color: #C5996A;
  --bg-color: #F2EDDC;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item .time-node {
  font-size: 28rpx;
  font-weight: bold;
  color: var(--color);
  text-align: center;
  display: flex;
  flex-flow: column;
  margin-top: 10rpx;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item .time-node .next {
  font-size: 24rpx;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item .header {
  display: flex;
  align-items: center;
  justify-content: center;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item .header .icon {
  width: 32rpx;
  height: 32rpx;
  margin-right: 6rpx;
}
.index-container .room-popup-container .body-wrapper .list-wrap .item .header .title {
  font-size: 28rpx;
  color: var(--color);
}
.index-container .room-popup-container .container {
  background: #FFFFFF;
  padding: 40rpx 32rpx 32rpx 32rpx;
}
.index-container .room-popup-container .container .date-wrap {
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 38rpx;
  background: #FFFFFF;
}
.index-container .room-popup-container .container .date-wrap .swiper-tab {
  width: 100%;
  display: flex;
  height: 120rpx;
}
.index-container .room-popup-container .container .date-wrap .swiper-tab .swiper-tab-list {
  flex: 1;
  text-align: center;
  color: #000;
  height: 100%;
  font-size: 24rpx;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
}
.index-container .room-popup-container .container .date-wrap .swiper-tab .swiper-tab-list .day {
  color: #000;
  font-size: 28rpx;
  margin-bottom: 2rpx;
}
.index-container .room-popup-container .container .date-wrap .swiper-tab .swiper-tab-list .icon {
  width: 25rpx;
  height: 29rpx;
  display: block;
  margin: 0 auto 10rpx;
}
.index-container .room-popup-container .container .date-wrap .swiper-tab .swiper-tab-list.on {
  background: #C5996A;
  color: #fff;
  border-radius: 20rpx 20rpx 0 0;
}
.index-container .room-popup-container .container .date-wrap .swiper-tab .swiper-tab-list.on .day {
  color: #fff;
}
.index-container .room-popup-container .container .date-wrap .date-item {
  display: flex;
  flex-flow: column;
  align-items: center;
  width: 112rpx;
  height: 112rpx;
  border-radius: 20rpx 20rpx 0 0;
  box-sizing: border-box;
  justify-content: center;
}
.index-container .room-popup-container .container .date-wrap .date-item.active {
  background: #C5996A;
}
.index-container .room-popup-container .container .date-wrap .date-item.active .date-value {
  color: #FFFFFF;
}
.index-container .room-popup-container .container .date-wrap .date-item .date-value {
  font-size: 28rpx;
  font-weight: bold;
  color: #333333;
  line-height: 33rpx;
}
.index-container .room-popup-container .container .notice-wrap {
  width: 100%;
  height: 48rpx;
  box-sizing: border-box;
  background: #F2EDDC;
  border-radius: 20rpx 20rpx 0 0;
  display: flex;
  align-items: center;
  padding: 0 22rpx;
}
.index-container .room-popup-container .container .notice-wrap .notice-value {
  font-size: 20rpx;
  font-weight: bold;
  color: #C5996A;
  margin-left: 12rpx;
}
.index-container .room-popup-container .container .notice-wrap .notice-icon {
  width: 32rpx;
  height: 32rpx;
}
.index-container .room-popup-container .container .header {
  height: 56rpx;
  font-size: 40rpx;
  font-weight: bold;
  color: #333333;
  line-height: 56rpx;
  text-align: center;
  margin-bottom: 60rpx;
}
.index-container .room-popup-container .close-icon {
  position: absolute;
  width: 40rpx;
  height: 40rpx;
  top: 40rpx;
  right: 32rpx;
}
.index-container .layer {
  width: 100%;
  height: 70vh;
  z-index: 9999;
  position: relative;
}
.index-container .layer_inner {
  width: 100%;
  height: 100%;
  border-radius: 12rpx 12rpx 0 0;
  position: absolute;
  bottom: 0;
  box-sizing: border-box;
  background: #fff;
  overflow: auto;
  padding-bottom: 92rpx;
}
.index-container .layer_inner .tips {
  display: flex;
  justify-content: center;
  color: #00B5DB;
  font-size: 28rpx;
  padding: 32rpx 18rpx 20rpx;
  border-bottom: 1rpx solid #E4E4E4;
  margin: 0 25rpx;
}
.index-container .layer_inner .tips image {
  width: 34rpx;
  height: 30rpx;
  margin-right: 16rpx;
}
.index-container .layer_inner .foot {
  bottom: 20rpx;
  width: 80%;
  left: 6%;
  background-color: #fff;
  position: fixed;
  padding: 32rpx;
  border-radius: 100rpx;
  justify-content: space-between;
}
.index-container .layer_inner .swiper-box {
  background-color: #f6f6f6;
}
.index-container .layer_inner .foot .btn {
  display: flex;
}
.index-container .layer_inner .foot_cancle,
.index-container .layer_inner .foot_confirm {
  text-align: center;
  color: #80BFCF;
  font-size: 28rpx;
  border-radius: 50rpx;
  padding: 16rpx;
  margin: 0 auto;
}
.index-container .layer_inner .foot_cancle {
  border: 1rpx solid #80BFCF;
}
.index-container .layer_inner .foot_confirm {
  background: #00B5DB;
  color: #FFF;
  margin-left: 12rpx;
}
.index-container .layer_inner .swiper-tab-list {
  border-radius: 30rpx;
  flex: 1;
  text-align: center;
  color: #000;
  height: 97rpx;
  font-size: 24rpx;
  padding-top: 33rpx;
}
.index-container .layer_inner .swiper-tab-list .day {
  color: #000;
  font-size: 28rpx;
  margin-bottom: 2rpx;
}
.index-container .layer_inner .swiper-tab-list .icon {
  width: 25rpx;
  height: 29rpx;
  display: block;
  margin: 0 auto 10rpx;
}
.index-container .layer_inner .swiper-tab-list.on {
  background: #00B5DB;
  color: #fff;
}
.index-container .layer_inner .swiper-tab-list.on .day {
  color: #fff;
}
.index-container .layer_inner .list {
  display: flex;
  flex-wrap: wrap;
}
.index-container .layer_inner .list_li {
  text-align: center;
  font-size: 24rpx;
  width: 162rpx;
  height: 52rpx;
  line-height: 52rpx;
  background: #80BFCF;
  border-radius: 12rpx;
  margin-left: 20rpx;
  margin-top: 14rpx;
}
.index-container .layer_inner .list_li.active {
  background: #00B5DB;
  color: #fff;
}
.index-container .layer_inner .list_li.disable {
  background: #ccc;
  color: #fff;
}
.index-container .layer .timeClick {
  height: 500rpx;
  background-color: #f6f6f6;
  margin-bottom: 200rpx;
  margin-top: 32rpx;
  padding: 32rpx;
}
.index-container .layer .timeClick .boxs {
  display: grid;
  justify-content: space-between;
  grid-template-columns: repeat(3, 30%);
}
.index-container .layer .timeClick .boxs image {
  width: 32rpx;
  margin-right: 12rpx;
}
.index-container .layer .timeClick .box {
  margin-bottom: 24rpx;
  width: 214rpx;
  height: 136rpx;
  background: #E7E7E7;
  border-radius: 20rpx;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 28rpx;
  color: #8C8C8C;
}
.index-container .layer .box.yellow {
  color: #E9A81F;
  background-color: #fff;
}
.index-container .layer .box.blue {
  color: #00b5db;
  background-color: #fff;
}
.index-container .layer .box.on {
  color: #fff;
  background-color: #00b5db;
}
.index-container .layer .box.yuding {
  color: #fff;
  background-color: #66d3e9;
}
.index-container .layer .timeClick .box .top {
  display: flex;
  align-items: center;
}
.index-container .pop-container {
  background-color: transparent;
}
.index-container .shop-list-container .scroll-view .card-conn-wrap {
  padding: 24rpx 32rpx 0;
  background-color: #f1f1f1;
}
.index-container .current-shop {
  padding: 38rpx 32rpx 32rpx;
  box-sizing: border-box;
  background: #FFFFFF;
  display: flex;
  flex-flow: column;
}
.index-container .current-shop .bottom-wrap {
  flex: 1;
  overflow: hidden;
  display: flex;
  justify-content: space-between;
}
.index-container .current-shop .bottom-wrap .left-info {
  display: flex;
  flex-flow: column;
}
.index-container .current-shop .bottom-wrap .left-info .address {
  height: 34rpx;
  font-size: 24rpx;
  font-weight: bold;
  color: #8C8C8C;
  line-height: 28rpx;
}
.index-container .current-shop .bottom-wrap .left-info .number-tag {
  margin-top: 32rpx;
  height: 40rpx;
  border-radius: 88rpx;
  border: 2rpx solid #C5996A;
  box-sizing: border-box;
  width: 168rpx;
  display: flex;
  align-items: center;
  justify-content: center;
}
.index-container .current-shop .bottom-wrap .left-info .number-tag .number {
  font-size: 20rpx;
  font-weight: bold;
  color: #C5996A;
}
.index-container .current-shop .bottom-wrap .right-info {
  display: flex;
  gap: 30rpx;
  align-items: center;
  padding-top: 30rpx;
  box-sizing: border-box;
  flex-shrink: 0;
}
.index-container .current-shop .bottom-wrap .right-info .icon-item {
  display: flex;
  align-items: center;
  flex-flow: column;
  justify-content: center;
  border: 0;
  background: none;
  margin: 0;
  padding: 0;
  outline: none;
}
.index-container .current-shop .bottom-wrap .right-info .icon-item::after {
  border: 0;
}
.index-container .current-shop .bottom-wrap .right-info .icon-item .icon {
  width: 56rpx;
  height: 56rpx;
}
.index-container .current-shop .bottom-wrap .right-info .icon-item .icon-text {
  height: 34rpx;
  font-size: 24rpx;
  font-weight: bold;
  margin-top: 4rpx;
  color: #333333;
  line-height: 28rpx;
}
.index-container .current-shop .top-wrap {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.index-container .current-shop .top-wrap .right-btn {
  width: 204rpx;
  height: 60rpx;
  background: #C5996A;
  border-radius: 88rpx;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: center;
  animation: scale 2s ease infinite;
  animation-direction: alternate;
}
.index-container .current-shop .top-wrap .right-btn .btn-title {
  font-size: 28rpx;
  font-weight: bold;
  color: #FFFFFF;
  height: 36rpx;
  line-height: 36rpx;
}
.index-container .current-shop .top-wrap .right-btn .chevron {
  width: 36rpx;
  height: 36rpx;
}
.index-container .current-shop .top-wrap .left-wrap {
  display: flex;
  align-items: center;
}
.index-container .current-shop .top-wrap .left-wrap .shop-name {
  height: 44rpx;
  font-size: 32rpx;
  font-weight: bold;
  color: #333333;
  line-height: 38rpx;
}
.index-container .current-shop .top-wrap .left-wrap .shop-img {
  width: 40rpx;
  height: 40rpx;
}
.index-container .header-wrapper {
  width: 100%;
  box-sizing: border-box;
  height: 466rpx;
}
.index-container .header-wrapper .header-body {
  position: absolute;
  top: 82rpx;
  z-index: 2;
  left: 0;
  right: 0;
}
.index-container .header-wrapper .header-body .header-top {
  padding: 0 40rpx;
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 2;
  width: 100%;
  background: transparent;
}
.index-container .header-wrapper .header-body .header-top .left-wrap {
  display: flex;
}
.index-container .header-wrapper .header-body .header-top .left-wrap .logo {
  width: 100rpx;
  height: 100rpx;
  margin-right: 24rpx;
}
.index-container .header-wrapper .header-body .header-top .left-wrap .tip {
  height: 48rpx;
  font-size: 16rpx;
  font-weight: 400;
  color: #333333;
  line-height: 48rpx;
  align-self: flex-end;
}
.index-container .header-wrapper .header-body .header-top .notice-icon {
  width: 34rpx;
  height: 34rpx;
}
.index-container .header-wrapper .header-body .header-menus {
  z-index: 2;
  margin-top: 26rpx;
  padding: 0 68rpx;
  box-sizing: border-box;
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  background: transparent;
}
.index-container .header-wrapper .header-body .header-menus .menu-item {
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  flex: 1;
}
.index-container .header-wrapper .header-body .header-menus .menu-item .menu-icon {
  width: 64rpx;
  height: 64rpx;
}
.index-container .header-wrapper .header-body .header-menus .menu-item .menu-title {
  font-size: 32rpx;
  font-weight: bold;
  color: #000000;
  margin-top: 12rpx;
}
.index-container .header-wrapper .bg {
  width: 100%;
  height: 466rpx;
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  z-index: 1;
}
.index-container .status-bar {
  width: 100%;
  height: var(--height);
}
.index-container .center-image {
  position: relative;
  font-size: 0;
}
.index-container .center-image .tag-container {
  position: absolute;
  bottom: 20rpx;
  left: 32rpx;
  display: flex;
  flex-flow: row nowrap;
}
.index-container .center-image .tag-container .tag-item {
  padding: 8rpx 20rpx;
  box-sizing: border-box;
  background: rgba(51, 51, 51, 0.7);
  border-radius: 88rpx;
  margin-right: 12rpx;
}
.index-container .center-image .tag-container .tag-item .tag {
  font-size: 20rpx;
  font-weight: bold;
  color: #FFFFFF;
  line-height: 23rpx;
}
.index-container .center-image .swiper {
  height: 276rpx;
}
.index-container .center-image .image {
  width: 100%;
  height: 276rpx;
}
