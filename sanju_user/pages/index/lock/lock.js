import {LOCK_ENUM} from '../../../constant/index'

Component({
    properties: {
        show: {
            type: Boolean,
            value: false,
        }
    },
    data: {},
    methods: {
        openLock(ev) {
            const {type} = ev.currentTarget.dataset
            if (!LOCK_ENUM.hasOwnProperty(type)) return
            this.triggerEvent('openLock', LOCK_ENUM[type])
        },
        onClose() {
            this.triggerEvent('close', false)
        },
    }
});
