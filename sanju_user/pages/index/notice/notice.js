Component({
    properties: {
        content:{
            type:String
        }
    },
    data: {},
    methods: {
        closeHandler() {
            this.triggerEvent('closePop', false)
        },
    }
});
