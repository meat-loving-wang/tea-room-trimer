export const SYSTEM_INFO = wx.getSystemInfoSync()

export const STATUS_HEIGHT = SYSTEM_INFO.statusBarHeight

export const LOCK_ENUM = {
    gate: '1',
    guard: '2',
}

export const BASE_URL = 'https://sanju.xinyunweb.com'

export const COUPON_ENUM = {
    GROUP: 'group',
    COUPON:'coupon'
}

export const WEEK_ENUM = {
    0: '周日',
    1: '周一',
    2: '周二',
    3: '周三',
    4: '周四',
    5: '周五',
    6: '周六'
};

export const SPEC_DATA =  [
    {
        label: '冷',
        value: 0
    },
    {
        label: '热',
        value: 1
    },
    {
        label: '常温',
        value: 2
    }
]

export const NAME_ENUM = 'order_cache'
