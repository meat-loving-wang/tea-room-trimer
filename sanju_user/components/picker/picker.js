// components/picker/picker.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pickerValue: [0, 0, 0],
    provinceDataList: [],
    cityDataList: [],
    areaDataList: [],
    showPicker: false,
    pickerValueDefault: [0, 0, 0]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  async creat(pickerValueDefault) {
    this.pickerValueDefault = pickerValueDefault;
    this.pickerValue = await this.handPickValueDefault();
    this._$emit('onConfirm');
  },
  show() {
    setTimeout(() => {
      this.showPicker = true;
    }, 0);
  },
  maskClick() {
    this.pickerCancel();
  },
  pickerCancel() {
    this.showPicker = false;
    this._$emit('onCancel');
  },
  pickerConfirm(e) {
    this.showPicker = false;
    this._$emit('onConfirm');
  },
  showPickerView() {
    this.showPicker = true;
  },
  async handPickValueDefault() {
    let tempPickerValue = this.pickerValueDefault;
    let provinceDataList=''
    app.ajax({url:'/address/area?pid=0',sucess:res=>{
      console.log(res)
      
    }})
    app.ajax({url:'/address/area?pid=' + (tempPickerValue[0] != 0 ? tempPickerValue[0] : this.provinceDataList[0].id),sucess:res=>{
      console.log(res)
    }})
    // this.provinceDataList = await this.$api.request('/address/area?pid=0');
    this.cityDataList = await this.$api.request('/address/area?pid=' + (tempPickerValue[0] != 0 ? tempPickerValue[0] : this.provinceDataList[0].id));
    this.areaDataList = await this.$api.request('/address/area?pid=' + (tempPickerValue[1] != 0 ? tempPickerValue[1] : this.cityDataList[0].id));

    for (let i in this.provinceDataList) {
      if (this.provinceDataList[i].id == tempPickerValue[0]) {
        tempPickerValue[0] = i;
        break;
      }
    }
    for (let i in this.cityDataList) {
      if (this.cityDataList[i].id == tempPickerValue[1]) {
        tempPickerValue[1] = i;
        break;
      }
    }
    for (let i in this.areaDataList) {
      if (this.areaDataList[i].id == tempPickerValue[2]) {
        tempPickerValue[2] = i;
        break;
      }
    }

    return tempPickerValue;
  },
  async pickerChange(e) {
    let changePickerValue = e.mp.detail.value;

    if (this.pickerValue[0] !== changePickerValue[0]) {
      // 第一级发生滚动
      // this.cityDataList = cityData[changePickerValue[0]];
      // this.areaDataList = areaData[changePickerValue[0]][0];

      let provinceId = this.provinceDataList[changePickerValue[0]].id;

      this.cityDataList = await this.$api.request('/address/area?pid=' + provinceId);
      this.areaDataList = await this.$api.request('/address/area?pid=' + this.cityDataList[0].id);

      changePickerValue[1] = 0;
      changePickerValue[2] = 0;
    } else if (this.pickerValue[1] !== changePickerValue[1]) {
      // 第二级滚动
      // this.areaDataList =
      // 	areaData[changePickerValue[0]][changePickerValue[1]];

      let cityId = this.cityDataList[changePickerValue[1]].id;
      this.areaDataList = await this.$api.request('/address/area?pid=' + cityId);

      changePickerValue[2] = 0;
    }
    this.pickerValue = changePickerValue;
    this._$emit('onChange');
  },
  _$emit(emitName) {
    let pickObj = {
      label: this._getLabel(),
      value: this._getAreaId(),
      cityCode: this._getCityCode()
    };
    this.$emit(emitName, pickObj);
  },
  _getLabel() {
    let pcikerLabel =
      this.provinceDataList[this.pickerValue[0]].label + '-' + this.cityDataList[this.pickerValue[1]].label + '-' + this.areaDataList[this.pickerValue[2]].label;
    return pcikerLabel;
  },
  _getCityCode() {
    return this.areaDataList[this.pickerValue[2]].value;
  },
  _getAreaId() {
    let areaId = [this.provinceDataList[this.pickerValue[0]].id, this.cityDataList[this.pickerValue[1]].id, this.areaDataList[this.pickerValue[2]].id];
    return areaId;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})