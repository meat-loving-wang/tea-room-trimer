import {BASE_URL} from '../../constant/index'

Component({
    properties: {
        cardData: {},
    },
    data: {
        id: '',
    },
    methods: {
        previewImage() {
            let {images_arr, image} = this.data.cardData
            if (!images_arr || !images_arr.length) {
                images_arr = [image]
            }
            const urls = images_arr.filter(Boolean).map(item => BASE_URL + item)
            wx.previewImage({
                urls
            })
        },
        booktime: function () {
            const {cardData: data} = this.data
            this.triggerEvent('booktime', data)
        },
        book: function () {
            const {cardData: data} = this.data
            let newdata = encodeURIComponent(JSON.stringify(data))
            const {id} = data
            const idStr = '&id=' + id
            wx.navigateTo({
                url: '/pages/book/book?newdata=' + newdata + idStr
            })
        },
    }
})
