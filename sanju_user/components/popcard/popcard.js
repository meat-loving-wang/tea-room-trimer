// components/popcard/popcard.js
Component({

  /**
   * 页面的初始数据
   */
  properties: {
    popshow: '',

  },
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  attached() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },
  methods: {
    close() {
      console.log("关闭")
      this.triggerEvent("cancel")
    },
    opensmall(){
      let that = this
      setTimeout(
        function () {
          that.triggerEvent("cancel")
        },
        2000,
        setTimeout(
          function () {
            that.triggerEvent("openDoorBsmall")
          }, 0)
      )
    },
    open() {
      console.log("开门")
      let that = this
      setTimeout(
        function () {
          that.triggerEvent("cancel")
        },
        2000,
        setTimeout(
          function () {
            that.triggerEvent("openDoorBig")
          }, 0)
      )
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
