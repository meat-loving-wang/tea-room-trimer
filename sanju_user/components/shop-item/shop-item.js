Component({
    properties: {},
    data: {
        progressData: []
    },
    pageLifetimes: {

    },
    lifetimes:{
        attached() {
            const value = this.getDateList()
            console.log(value, 'value')
            this.setData({
                progressData: [...value]
            })
        }
    },
    methods: {
        reserveHandler(){
          wx.navigateTo({
              url:'/pages/confirm-order/confirm-order'
          })
        },
        getDateList() {
            const date = new Date()
            const currentHour = date.getHours()
            const before = [], after = [];
            for (let i = 1; i < 24; i++) {
                if (i < currentHour) {
                    after.push({
                        timeNode: i,
                    })
                } else {
                    before.push({
                        timeNode: i,
                    })
                }
            }
            return [...before, {timeNode: '次'}, ...after]
        },


    },
});
