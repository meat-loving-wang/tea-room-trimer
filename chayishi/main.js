import App from './App'
import https from '@/common/https.js'
import http from '@/common/http.js'
import uView from '@/uni_modules/uview-ui'
import config from './config'
// #ifndef VUE3
import Vue from 'vue'
Vue.prototype.https = https
Vue.prototype.http = http
Vue.prototype.topBarTop = function() {
	// #ifdef MP-WEIXIN
	return uni.getMenuButtonBoundingClientRect().top;
	// #endif
	// #ifndef MP-WEIXIN
	const SystemInfo = uni.getSystemInfoSync();
	return SystemInfo.statusBarHeight;
	// #endif
};
Vue.use(uView)
Vue.prototype.goPath = function(path) {
	uni.navigateTo({
		url: path
	})
}
Vue.prototype.goLogins = function() {
	uni.navigateTo({
		url: '/pages/login/login'
	})
}
Vue.config.productionTip = false

App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}

// #endif
