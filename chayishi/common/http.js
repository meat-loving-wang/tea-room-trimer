import config from '@/config.js'
// let httpsUrl = 'https://chazhichashi.rchz.top'
let httpsUrl = 'https://sanju.xinyunweb.com'
const http = function(path, data, method) {
	data.token = uni.getStorageSync('token') || '';
	data.app_id = config.app_id || 10001;
	data = data || {};
	return new Promise((result, reject) => {
		uni.request({
			url: httpsUrl + path,
			data: data,
			dataType: 'json',
			method: method || 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			success: (res) => {
				if (res.statusCode !== 200 || typeof res.data !== 'object') {
					return false;
				}
				if (res.data.code === -1) {
					// 登录态失效, 重新登录
					uni.navigateTo({
						url: 'pages/login/login'
					})
				} else if (res.data.code === 0) {
					reject(res.data)
					return false;
				} else {
					result(res.data)
				}
			},
			fail: (res) => {
				reject(res.data)
			},
			complete: (res) => {

			}
		})
	})
}
export default http;