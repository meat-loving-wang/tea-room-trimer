export function toast({ title, time = 1000, icon = 'none' }, ...arg) {
	if (!title) return
	uni.showToast({
		title,
		icon,
		duration: time
	})
	const [callback] = arg
	if (!!callback) {
		let timeID = setTimeout(() => {
			clearTimeout(timeID)
			timeID = null
			callback()
		}, time)
	}
}

export function patchZero(value) {
	if (isNaN(value)) return '0'
	return ('0' + value).slice(-2)
}

export function delay(time = 300) {
	return new Promise(resolve => {
		let timeID = setTimeout(() => {
			clearTimeout(timeID)
			timeID = null
			resolve()
		}, time)
	})
}