(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "pages/data-board/data-board" ], {
    "1bda": function bda(e, t, r) {
        "use strict";
        r.r(t);
        var n = r("5c7b"), i = r.n(n);
        for (var a in n) [ "default" ].indexOf(a) < 0 && function(e) {
            r.d(t, e, function() {
                return n[e];
            });
        }(a);
        t["default"] = i.a;
    },
    "2c19": function c19(e, t, r) {
        "use strict";
        (function(e, t) {
            var n = r("4ea4");
            r("f9fc");
            n(r("66fd"));
            var i = n(r("61a9"));
            e.__webpack_require_UNI_MP_PLUGIN__ = r, t(i.default);
        }).call(this, r("bc2e")["default"], r("543d")["createPage"]);
    },
    "5c7b": function c7b(e, t, r) {
        "use strict";
        (function(e) {
            var n = r("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = n(r("2eee")), a = n(r("c973")), o = n(r("9523")), c = r("26cb");
            function u(e, t) {
                var r = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), r.push.apply(r, n);
                }
                return r;
            }
            var s = {
                computed: function(e) {
                    for (var t = 1; t < arguments.length; t++) {
                        var r = null != arguments[t] ? arguments[t] : {};
                        t % 2 ? u(Object(r), !0).forEach(function(t) {
                            (0, o.default)(e, t, r[t]);
                        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(r)) : u(Object(r)).forEach(function(t) {
                            Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(r, t));
                        });
                    }
                    return e;
                }({}, (0, c.mapGetters)([ "token", "activeStore", "userInfo" ])),
                onShow: function onShow() {
                    if (!this.token.length) return this.$toast({
                        title: "请先登录",
                        time: 2e3
                    }, function() {
                        e.reLaunch({
                            url: "/pages/public/login"
                        });
                    });
                    this.initDate(), this.initData();
                },
                methods: {
                    getDateByStr: function getDateByStr(e) {
                        var t = new Date(e), r = [ t.getFullYear(), t.getMonth() + 1, t.getDate() ], n = r[0], i = r[1], a = r[2], o = "".concat(n, "-").concat(this.patchZero(i), "-").concat(this.patchZero(a));
                        return {
                            str: o,
                            value: t.getTime()
                        };
                    },
                    dateChange: function dateChange(e) {
                        var t = [ "orderDate", "moneyDate" ][this.selectType], r = this.getDateByStr(e.replace(/\-/g, "/")), n = r.str, i = r.value;
                        this[t] = n, this[t + "Value"] = i;
                        this.getDateByDate(n, [ "order", "money" ][this.selectType], !0), this.single = 0, 
                        console.log(this.formatDate(this.orderDateValue), "orderDate"), console.log(this.formatDate(this.moneyDateValue), "moneyDate");
                    },
                    selectDate: function selectDate(e) {
                        var t = this;
                        this.selectType = e;
                        var r = [ "orderDateValue", "moneyDateValue" ][e], n = this[r];
                        this.single = n, this.$forceUpdate(), setTimeout(function() {
                            t.$refs.datePicker.show();
                        }, 50);
                    },
                    delay: function delay() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 200;
                        return new Promise(function(t) {
                            setTimeout(function() {
                                t();
                            }, e);
                        });
                    },
                    getDateByDate: function getDateByDate(t) {
                        var r = arguments, n = this;
                        return (0, a.default)(i.default.mark(function a() {
                            var o, c, u, s, h, d, l, _, f, p, m, y, g, D, b, v, w, x, O, k, j, P, V, S, T, $, M, Z, B, I, L;
                            return i.default.wrap(function(i) {
                                while (1) switch (i.prev = i.next) {
                                  case 0:
                                    if (o = r.length > 1 && void 0 !== r[1] ? r[1] : "all", c = r.length > 2 && void 0 !== r[2] && r[2], 
                                    u = n.activeStore, -1 !== u.id) {
                                        i.next = 5;
                                        break;
                                    }
                                    return i.abrupt("return", n.$toast({
                                        title: "未选择店铺",
                                        time: 2e3
                                    }));

                                  case 5:
                                    if (s = n.userInfo.mobile, t) {
                                        i.next = 8;
                                        break;
                                    }
                                    return i.abrupt("return", n.$toast({
                                        title: "未选择日期"
                                    }));

                                  case 8:
                                    return c && e.showLoading({
                                        title: "正在获取数据",
                                        mask: !0
                                    }), i.next = 11, n.https("/shujutongji/lishitongji", {
                                        store_id: u.id,
                                        mobile: s,
                                        date: t
                                    });

                                  case 11:
                                    if (h = i.sent, d = h.code, l = h.data, h.msg, 1 === d) {
                                        i.next = 17;
                                        break;
                                    }
                                    return e.hideLoading(), n.$toast({
                                        title: "获取数据失败"
                                    }), i.abrupt("return");

                                  case 17:
                                    if (!c) {
                                        i.next = 21;
                                        break;
                                    }
                                    return i.next = 20, n.delay(200);

                                  case 20:
                                    e.hideLoading();

                                  case 21:
                                    for (_ in l) f = l[_], l[_] = (1 * f).toFixed(2);
                                    if (p = l.chashi_order_num_wx, m = l.chashi_order_num_yue, y = l.chashi_order_num_meituan, 
                                    g = l.chashi_order_num_douyin, D = l.shipin_order_num, b = l.xudan_order_num, v = l.chongzhi_num, 
                                    w = l.chayishi_num, x = l.chashi_order_price_weixin, O = l.chashi_order_price_yue, 
                                    k = l.chashi_order_price_meituan, j = l.chashi_order_price_douyin, P = l.shipin_order_price, 
                                    V = l.xudan_order_price, S = l.xudan_order_price_wx, T = l.xudan_order_price_yue, 
                                    $ = l.chongzhi_price, M = l.chayishi_price, Z = l.chashi_order_price, B = l.weixin_pay_price, 
                                    I = {
                                        chashi_order_num_wx: p,
                                        chashi_order_num_yue: m,
                                        chashi_order_num_meituan: y,
                                        chashi_order_num_douyin: g,
                                        shipin_order_num: D,
                                        xudan_order_num: b,
                                        chongzhi_num: v,
                                        chayishi_num: w
                                    }, L = {
                                        chashi_order_price_weixin: x,
                                        chashi_order_price_yue: O,
                                        chashi_order_price_meituan: k,
                                        chashi_order_price_douyin: j,
                                        shipin_order_price: P,
                                        xudan_order_price: V,
                                        chongzhi_price: $,
                                        chayishi_price: M,
                                        xudan_order_price_wx: S,
                                        xudan_order_price_yue: T,
                                        chashi_order_price: Z,
                                        weixin_pay_price: B
                                    }, "order" !== o) {
                                        i.next = 28;
                                        break;
                                    }
                                    return n.historyOrder = I, i.abrupt("return");

                                  case 28:
                                    if ("money" !== o) {
                                        i.next = 31;
                                        break;
                                    }
                                    return n.historyMoney = L, i.abrupt("return");

                                  case 31:
                                    "all" === o && (n.historyOrder = I, n.historyMoney = L);

                                  case 32:
                                  case "end":
                                    return i.stop();
                                }
                            }, a);
                        }))();
                    },
                    initData: function initData() {
                        var e = this;
                        return (0, a.default)(i.default.mark(function t() {
                            var r, n;
                            return i.default.wrap(function(t) {
                                while (1) switch (t.prev = t.next) {
                                  case 0:
                                    if (r = e.activeStore, -1 !== r.id) {
                                        t.next = 3;
                                        break;
                                    }
                                    return t.abrupt("return", e.$toast({
                                        title: "未选择店铺",
                                        time: 2e3
                                    }));

                                  case 3:
                                    n = e.userInfo.mobile, e.https("/shujutongji/index", {
                                        store_id: r.id,
                                        mobile: n
                                    }).then(function(t) {
                                        var r = t.code, n = t.data, i = t.msg;
                                        if (1 !== r) return e.$toast({
                                            title: i
                                        });
                                        e.$toast({
                                            title: "数据更新成功"
                                        });
                                        var a = n.todaydingdan, o = n.todayshouru;
                                        e.ding_dan = a, e.shou_ru = o;
                                    });

                                  case 5:
                                  case "end":
                                    return t.stop();
                                }
                            }, t);
                        }))();
                    },
                    patchZero: function patchZero(e) {
                        return ("0" + e).slice(-2);
                    },
                    formatDate: function formatDate() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0, t = new Date(0 === e ? Date.now() : e), r = [ t.getFullYear(), t.getMonth() + 1, t.getDate() ], n = r[0], i = r[1], a = r[2], o = "".concat(n, "-").concat(this.patchZero(i), "-").concat(this.patchZero(a));
                        return {
                            str: o,
                            value: t.getTime()
                        };
                    },
                    initDate: function initDate() {
                        var e = Date.now() - 864e5, t = this.formatDate(e), r = t.str, n = t.value;
                        this.endDate = n, this.single = n, this.endStr = r, this.moneyDate = r, this.orderDate = r, 
                        this.orderDateValue = n, this.moneyDateValue = n, this.token.length && this.getDateByDate(r, "all", !1);
                    },
                    getNowDate: function getNowDate() {
                        var e = new Date(Date.now() - 864e5), t = [ e.getFullYear(), e.getMonth() + 1, e.getDate() ], r = t[0], n = t[1], i = t[2], a = "".concat(r, "-").concat(this.patchZero(n), "-").concat(this.patchZero(i));
                        this.orderDate = a, this.moneyDate = a, this.orderDateValue = e.getTime(), this.moneyDateValue = e.getTime();
                    }
                },
                data: function data() {
                    return {
                        ding_dan: {},
                        shou_ru: {},
                        single: 0,
                        endDate: 0,
                        orderDate: "",
                        moneyDate: "",
                        orderDateValue: 0,
                        moneyDateValue: 0,
                        endStr: "",
                        selectType: 0,
                        historyOrder: {},
                        historyMoney: {}
                    };
                }
            };
            t.default = s;
        }).call(this, r("543d")["default"]);
    },
    "61a9": function a9(e, t, r) {
        "use strict";
        r.r(t);
        var n = r("94cf"), i = r("1bda");
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(e) {
            r.d(t, e, function() {
                return i[e];
            });
        }(a);
        r("c26e");
        var o = r("f0c5"), c = Object(o["a"])(i["default"], n["b"], n["c"], !1, null, "02f250c2", null, !1, n["a"], void 0);
        t["default"] = c.exports;
    },
    9165: function _(e, t, r) {},
    "94cf": function cf(e, t, r) {
        "use strict";
        r.d(t, "b", function() {
            return i;
        }), r.d(t, "c", function() {
            return a;
        }), r.d(t, "a", function() {
            return n;
        });
        var n = {
            uniDatetimePicker: function uniDatetimePicker() {
                return Promise.all([ r.e("common/vendor"), r.e("uni_modules/uni-datetime-picker/components/uni-datetime-picker/uni-datetime-picker") ]).then(r.bind(null, "45dc"));
            }
        }, i = function i() {
            var e = this.$createElement;
            this._self._c;
        }, a = [];
    },
    c26e: function c26e(e, t, r) {
        "use strict";
        var n = r("9165"), i = r.n(n);
        i.a;
    }
}, [ [ "2c19", "common/runtime", "common/vendor" ] ] ]);