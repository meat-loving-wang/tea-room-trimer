const {
    request
} = require("../../utils/request.js")
Page({
    data: {
        dingDanDateText: "2023-02-01",
        lishiDateText: "2024-02-01",
        dingDanDateStart: "",
        dingDanDateEnd: "",
        lishiDateStart: "",
        lishiDateEnd: "",
        activeStoreId: wx.getStorageSync('active_id') || "",
        userInfo: wx.getStorageSync('userinfo') || null
    },
    onLoad() {
        this.initDate()
    },
    initDate() {
        const date = new Date()

        function f(t) {
            function format(v) {
                return ('0' + v).slice(-2)
            }
            const date = new Date(t)
            const [year, month, day] = [date.getFullYear(), date.getMonth() + 1, date.getDate()]
            return `${year}-${format(month)}-${format(day)}`
        }
        const [y, m] = [date.getFullYear(), date.getMonth()]
        const firstDay = new Date(y, m, 1)
        const endDay = new Date(y, m + 1, 0)
        const startDate = f(firstDay),
            endDate = f(endDay);
        console.log(startDate, 'startDate', endDate, 'endDate');
        this.setData({
            dingDanDateStart: startDate,
            dingDanDateEnd: endDate,
            lishiDateStart: startDate,
            lishiDateEnd: endDate,
        })
    },
    dingDanStartDateChange(ev){
        const {
            value
        } = ev.detail
        this.setData({
            dingDanDateStart: value,
        })
    },
    dingDanEndDateChange(ev){
        const {
            value
        } = ev.detail
        this.setData({
            dingDanDateEnd: value,
        })
    },
    lishiEndDateChange(ev){
        const {
            value
        } = ev.detail
        this.setData({
            lishiDateEnd: value,
        })
    },
    lishiDateStartChange(ev) {
        const {
            value
        } = ev.detail
        this.setData({
            lishiDateStart: value,
        })
    },
})