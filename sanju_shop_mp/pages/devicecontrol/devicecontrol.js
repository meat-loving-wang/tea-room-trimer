(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "pages/devicecontrol/devicecontrol" ], {
    "0381": function _(e, t, i) {},
    "1a77": function a77(e, t, i) {
        "use strict";
        var o = i("0381"), n = i.n(o);
        n.a;
    },
    "42c6": function c6(e, t, i) {
        "use strict";
        i.r(t);
        var o = i("adf2"), n = i("cbdd");
        for (var c in n) [ "default" ].indexOf(c) < 0 && function(e) {
            i.d(t, e, function() {
                return n[e];
            });
        }(c);
        i("1a77");
        var s = i("f0c5"), a = Object(s["a"])(n["default"], o["b"], o["c"], !1, null, null, null, !1, o["a"], void 0);
        t["default"] = a.exports;
    },
    "53cc": function cc(e, t, i) {
        "use strict";
        (function(e, o) {
            var n = i("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var c = n(i("9523")), s = i("26cb");
            function a(e, t) {
                var i = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), i.push.apply(i, o);
                }
                return i;
            }
            var r = {
                data: function data() {
                    return {
                        pages: 1,
                        storeId: e.getStorageSync("storeListId")[0],
                        last_page: null,
                        roomList: [],
                        mobile: e.getStorageSync("userinfo").mobile,
                        lockId: "",
                        blueYaCode: "",
                        deviceId: "",
                        servicesUUID: "",
                        delayTime: 300,
                        notifyCharacteristicId: "",
                        writeCharacteristicId: "",
                        characteristics: [],
                        isNotifyReturn: !1,
                        lock_brand: !0,
                        maxFailNums: 3,
                        failNums: 0
                    };
                },
                computed: function(e) {
                    for (var t = 1; t < arguments.length; t++) {
                        var i = null != arguments[t] ? arguments[t] : {};
                        t % 2 ? a(Object(i), !0).forEach(function(t) {
                            (0, c.default)(e, t, i[t]);
                        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(i)) : a(Object(i)).forEach(function(t) {
                            Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(i, t));
                        });
                    }
                    return e;
                }({}, (0, s.mapGetters)([ "activeStore" ])),
                onLoad: function onLoad() {
                    this.getRoomList();
                },
                onReachBottom: function onReachBottom() {
                    this.pages < this.last_page && (this.pages += 1, this.getRoomList());
                },
                methods: {
                    getRoomList: function getRoomList() {
                        var t = this, i = this.activeStore.id;
                        if (-1 === i) return o.showToast({
                            title: "您没有选择店铺",
                            duration: 2e3,
                            icon: "error"
                        }), void setTimeout(function() {
                            e.navigateBack();
                        }, 2e3);
                        this.https("/room/lists", {
                            store_id: i,
                            page: this.pages
                        }).then(function(e) {
                            console.log(e, "resp"), 1 == t.pages ? t.roomList = e.data.data : t.roomList = t.roomList.concat(e.data.data), 
                            t.last_page = e.data.last_page;
                        }).catch(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        });
                    },
                    openRoomLock: function openRoomLock(t) {
                        var i = this, n = this, c = this.activeStore.id;
                        e.showLoading({
                            mask: !0,
                            title: "正在尝试开锁...."
                        }), this.https("/lock/open", {
                            type: 2,
                            storeId: c,
                            roomId: t.id,
                            userId: e.getStorageSync("userinfo").id,
                            mobile: this.mobile
                        }, "GET").then(function(t) {
                            console.log("then"), e.hideLoading();
                            t.code;
                            var c = t.msg;
                            e.showToast({
                                title: c,
                                icon: "none",
                                duration: 2e3
                            }), i.blueYaCode = t.data.key, i.lockId = t.data.lockId, 2 == t.code && setTimeout(function() {
                                i.blueYaCode && o.openBluetoothAdapter({
                                    success: function success(e) {
                                        setTimeout(function() {
                                            n.findBlue();
                                        }, "android" == n.isModel() ? n.delayTime : 0);
                                    },
                                    fail: function fail(e) {
                                        o.showToast({
                                            title: "请开启蓝牙,部分安卓手机需要开启定位服务",
                                            icon: "none",
                                            duration: 800
                                        });
                                    }
                                });
                            }, 1e3);
                        }).catch(function(t) {
                            console.log(t, "eror"), e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        }).finally(function() {
                            e.hideLoading();
                        });
                    },
                    openBig: function openBig() {
                        var t = this, i = this.activeStore.id, n = this;
                        e.showLoading({
                            mask: !0,
                            title: "正在尝试开锁...."
                        }), this.https("/lock/open", {
                            type: 1,
                            storeId: i,
                            userId: e.getStorageSync("userinfo").id,
                            mobile: this.mobile
                        }, "GET").then(function(i) {
                            e.hideLoading(), t.blueYaCode = i.data.key, t.lockId = i.data.lockId, 2 == i.code && setTimeout(function() {
                                t.blueYaCode && o.openBluetoothAdapter({
                                    success: function success(e) {
                                        setTimeout(function() {
                                            n.findBlue();
                                        }, "android" == n.isModel() ? n.delayTime : 0);
                                    },
                                    fail: function fail(e) {
                                        o.showToast({
                                            title: "请开启蓝牙,部分安卓手机需要开启定位服务",
                                            icon: "none",
                                            duration: 800
                                        });
                                    }
                                });
                            }, 1e3);
                        }).catch(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        });
                    },
                    findBlue: function findBlue() {
                        var e = this;
                        o.startBluetoothDevicesDiscovery({
                            allowDuplicatesKey: !1,
                            interval: 0,
                            success: function success(t) {
                                setTimeout(function() {
                                    e.getBlueYaList();
                                }, 2e3), o.showLoading({
                                    title: "正在搜索设备"
                                });
                            },
                            fail: function fail(e) {
                                this.bluetoothFailAgain();
                            }
                        });
                    },
                    getBlueYaList: function getBlueYaList() {
                        var e = this;
                        o.getBluetoothDevices({
                            success: function success(t) {
                                o.hideLoading(), e.findDevice(t.devices);
                            }
                        });
                    },
                    ab2hex: function ab2hex(e) {
                        var t = Array.prototype.map.call(new Uint8Array(e), function(e) {
                            return ("00" + e.toString(16)).slice(-2);
                        });
                        return t.join("");
                    },
                    stringToHexBuffer: function stringToHexBuffer(e) {
                        var t = new Uint8Array(e.match(/[\da-f]{2}/gi).map(function(e) {
                            return console.log("转数据1"), parseInt(e, 16);
                        }));
                        return console.log("转数据2"), t.buffer;
                    },
                    findDevice: function findDevice(e) {
                        var t = this, i = this;
                        e.forEach(function(e) {
                            var o = i.ab2hex(e.advertisData), n = function(e) {
                                var t = e.split(".");
                                return t.map(function(e) {
                                    var t = Number(e).toString(16);
                                    return t.length < 2 && (t = "0" + t), t;
                                }).join("");
                            }(i.lockId);
                            console.log(n === o, "判断"), e.advertisData && i.ab2hex(e.advertisData) === n && (t.deviceId = e.deviceId, 
                            i.connectBlueYa(e.deviceId));
                        });
                    },
                    openBigDoor: function openBigDoor() {
                        var t = this.activeStore.id;
                        this.https("/lock/open", {
                            type: 1,
                            storeId: t,
                            userId: e.getStorageSync("userinfo").id,
                            mobile: this.mobile
                        }, "GET").then(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        }).catch(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        });
                    },
                    connectBlueYa: function connectBlueYa(e) {
                        var t = this;
                        o.createBLEConnection({
                            deviceId: e,
                            success: function success(i) {
                                console.log(i, "连接状态"), 0 == i.errCode ? (o.showToast({
                                    title: "蓝牙连接成功",
                                    icon: "none",
                                    duration: 800
                                }), t.getBLEDeviceServices(e)) : o.showToast({
                                    title: "蓝牙连接失败",
                                    icon: "none",
                                    duration: 800
                                }), o.stopBluetoothDevicesDiscovery({
                                    success: function success(e) {
                                        console.log("成功关闭蓝牙搜索功能");
                                    }
                                });
                            }
                        });
                    },
                    getBLEDeviceServices: function getBLEDeviceServices(e) {
                        var t = this, i = this;
                        o.getBLEDeviceServices({
                            deviceId: e,
                            success: function success(e) {
                                console.log("getBLEDeviceServices", e);
                                for (var o = 0; o < e.services.length; o++) "0000FFF0-0000-1000-8000-00805F9B34FB" == e.services[o].uuid && (t.servicesUUID = e.services[o].uuid);
                                i.getBLEDeviceCharacteristics();
                            },
                            fail: function fail(e) {
                                console.log("getBLEDeviceServices fail", e), i.bluetoothFailAgain();
                            }
                        });
                    },
                    getBLEDeviceCharacteristics: function getBLEDeviceCharacteristics() {
                        var e = this, t = this;
                        o.getBLEDeviceCharacteristics({
                            deviceId: t.deviceId,
                            serviceId: t.servicesUUID,
                            success: function success(e) {
                                console.log(e.characteristics, "数据");
                                for (var i = 0; i < e.characteristics.length; i++) e.characteristics[i].properties.notify && (console.log("开启notify的characteristicId:" + e.characteristics[i].uuid), 
                                t.notifyCharacteristicId = e.characteristics[i].uuid), e.characteristics[i].properties.write && (console.log("开启write的characteristicId:" + e.characteristics[i].uuid), 
                                t.writeCharacteristicId = e.characteristics[i].uuid), e.characteristics[i].properties.read && console.log("开启read的characteristicId:" + e.characteristics[i].uuid);
                                console.log(t.writeCharacteristicId, "写入特征值ID"), console.log(t.notifyCharacteristicId, "开启notify的ID"), 
                                t.characteristics = e.characteristics, t.notifyBLECharacteristicValueChange();
                            },
                            fail: function fail(t) {
                                console.log("失败了"), e.bluetoothFailAgain();
                            },
                            complete: function complete(e) {}
                        });
                    },
                    notifyBLECharacteristicValueChange: function notifyBLECharacteristicValueChange() {
                        var e = this;
                        var t = this;
                        o.onBLECharacteristicValueChange(function(e) {
                            console.log(e, "监听到返回的数据"), o.showToast({
                                title: "监听到返回的数据",
                                icon: "none",
                                duration: 800
                            }), t.isNotifyReturn = !0;
                            var i = function(e) {
                                return Array.prototype.map.call(new Uint8Array(e), function(e) {
                                    return ("00" + e.toString(16)).slice(-2);
                                }).join("");
                            }(e.value), n = i.indexOf("1a0794aa") > -1;
                            if (n && t.lock_brand) return o.showToast({
                                title: "开锁成功",
                                icon: "none",
                                duration: 800
                            }), void t.writeBLECharacteristicValueDelete(key);
                        }), this.lock_brand && o.notifyBLECharacteristicValueChange({
                            deviceId: t.deviceId,
                            serviceId: t.servicesUUID,
                            characteristicId: t.notifyCharacteristicId,
                            state: !0,
                            success: function success(e) {},
                            fail: function fail(e) {}
                        }), o.notifyBLECharacteristicValueChange({
                            deviceId: t.deviceId,
                            serviceId: t.servicesUUID,
                            characteristicId: t.notifyCharacteristicId,
                            state: !0,
                            success: function success(e) {
                                "android" == t.isModel() ? setTimeout(function() {
                                    t.isNotifyReturn = !1, t.writeBLECharacteristicValue();
                                }, t.delayTime) : (o.showToast({
                                    title: "苹果设备",
                                    icon: "none",
                                    duration: 800
                                }), t.isNotifyReturn = !1, t.writeBLECharacteristicValue());
                            },
                            fail: function fail(t) {
                                console.log("通知打开失败", 4004), e.bluetoothFailAgain();
                            }
                        });
                    },
                    writeBLECharacteristicValueDelete: function writeBLECharacteristicValueDelete(e) {
                        var t = [];
                        while (null != e && e.length) {
                            var i = e.substring(0, 2), n = parseInt(i, 16);
                            t.push(n), e = e.substring(2);
                        }
                        var c = new Uint8Array(t).buffer;
                        console.log(c, "arrayBuffer"), o.writeBLECharacteristicValue({
                            deviceId: this.device.deviceId,
                            serviceId: this.device.servicesUUID,
                            characteristicId: this.writeCharacteristicId,
                            value: c,
                            success: function success(e) {
                                console.log(e, "res");
                            },
                            fail: function fail(e) {
                                console.log(e, "res");
                            }
                        });
                    },
                    writeBLECharacteristicValue: function writeBLECharacteristicValue() {
                        var e = this, t = "BABEC0DE" + e.blueYaCode, i = t.toLocaleLowerCase(), n = "";
                        console.log("往锁写入的数据：", i);
                        var c = 0;
                        (function t() {
                            var s = this;
                            console.log("次数" + c), n = e.stringToHexBuffer(i), o.writeBLECharacteristicValue({
                                deviceId: e.deviceId,
                                serviceId: e.servicesUUID,
                                characteristicId: e.writeCharacteristicId,
                                value: n,
                                success: function success() {
                                    e.lock_brand ? o.showToast({
                                        title: "写入数据完成",
                                        icon: "none",
                                        duration: 800
                                    }) : (console.log("锁写入数据剩余：".concat(i.length - 0)), 0 == i.length || e.isNotifyReturn ? o.showToast({
                                        title: "写入数据完成",
                                        icon: "none",
                                        duration: 800
                                    }) : (32, t()));
                                },
                                fail: function fail(i) {
                                    c++, c > 2 && !e.isNotifyReturn ? s.bluetoothFailAgain() : e.isNotifyReturn || t(), 
                                    console.log("写入数据失败", i);
                                }
                            });
                        })();
                    },
                    bluetoothFailAgain: function bluetoothFailAgain() {
                        var e = this;
                        this.stopBluetoothDevicesDiscovery(), this.closeBLEConnection(), this.closeBluetoothAdapter(function() {
                            e.failNums += 1, console.log("失败次数:", e.failNums), e.failNums === e.maxFailNums ? console.log("到失败次数上限:", e.maxFailNums) : e.failNums < e.maxFailNums && setTimeout(function() {
                                console.log("失败后重新开始"), e.openLock(e.device);
                            }, 200);
                        });
                    },
                    closeLock: function closeLock(e) {
                        this.close(e);
                    },
                    open: function open(t) {
                        this.https("/room/openCharge", {
                            id: t
                        }).then(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        }).catch(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        });
                    },
                    close: function close(t) {
                        this.https("/room/closeCharge", {
                            id: t
                        }).then(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        }).catch(function(t) {
                            e.showToast({
                                title: t.msg,
                                icon: "none"
                            });
                        });
                    },
                    openKongtiao: function openKongtiao(e) {
                        this.open(e);
                    },
                    closeKongtiao: function closeKongtiao(e) {
                        this.close(e);
                    },
                    openChazuo: function openChazuo(e) {
                        this.open(e);
                    },
                    closeChazuo: function closeChazuo(e) {
                        this.close(e);
                    },
                    openDong: function openDong(e) {
                        this.open(e);
                    },
                    closeDong: function closeDong(e) {
                        this.close(e);
                    },
                    isModel: function isModel() {
                        var e = o.getSystemInfoSync();
                        return console.log("ios" == e.platform, "苹果"), console.log("android" == e.platform, "安卓"), 
                        console.log("devtools" == e.platform, "模拟器"), e.res;
                    },
                    stopBluetoothDevicesDiscovery: function stopBluetoothDevicesDiscovery() {
                        var e = this;
                        o.stopBluetoothDevicesDiscovery({
                            complete: function complete() {
                                e._discoveryStarted = !1;
                            }
                        });
                    },
                    closeBLEConnection: function closeBLEConnection() {
                        var e = this;
                        this.deviceId && o.closeBLEConnection({
                            deviceId: this.deviceId,
                            success: function success() {
                                e.log("成功断开连接");
                            }
                        });
                    }
                }
            };
            t.default = r;
        }).call(this, i("543d")["default"], i("bc2e")["default"]);
    },
    adf2: function adf2(e, t, i) {
        "use strict";
        i.d(t, "b", function() {
            return o;
        }), i.d(t, "c", function() {
            return n;
        }), i.d(t, "a", function() {});
        var o = function o() {
            var e = this.$createElement;
            this._self._c;
        }, n = [];
    },
    cbdd: function cbdd(e, t, i) {
        "use strict";
        i.r(t);
        var o = i("53cc"), n = i.n(o);
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(e) {
            i.d(t, e, function() {
                return o[e];
            });
        }(c);
        t["default"] = n.a;
    },
    d74d: function d74d(e, t, i) {
        "use strict";
        (function(e, t) {
            var o = i("4ea4");
            i("f9fc");
            o(i("66fd"));
            var n = o(i("42c6"));
            e.__webpack_require_UNI_MP_PLUGIN__ = i, t(n.default);
        }).call(this, i("bc2e")["default"], i("543d")["createPage"]);
    }
}, [ [ "d74d", "common/runtime", "common/vendor" ] ] ]);