(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "pages/cleaningtasks/cleaningtasks" ], {
    "60df": function df(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return a;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {
            return r;
        });
        var r = {
            uTabs: function uTabs() {
                return Promise.all([ n.e("common/vendor"), n.e("uni_modules/uview-ui/components/u-tabs/u-tabs") ]).then(n.bind(null, "7564"));
            }
        }, a = function a() {
            var e = this.$createElement;
            this._self._c;
        }, i = [];
    },
    9661: function _(e, t, n) {
        "use strict";
        var r = n("4ea4");
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var a = r(n("2eee")), i = r(n("c973")), c = r(n("278c")), u = r(n("9523")), o = n("26cb");
        function s(e, t) {
            var n = Object.keys(e);
            if (Object.getOwnPropertySymbols) {
                var r = Object.getOwnPropertySymbols(e);
                t && (r = r.filter(function(t) {
                    return Object.getOwnPropertyDescriptor(e, t).enumerable;
                })), n.push.apply(n, r);
            }
            return n;
        }
        function f(e) {
            for (var t = 1; t < arguments.length; t++) {
                var n = null != arguments[t] ? arguments[t] : {};
                t % 2 ? s(Object(n), !0).forEach(function(t) {
                    (0, u.default)(e, t, n[t]);
                }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : s(Object(n)).forEach(function(t) {
                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                });
            }
            return e;
        }
        var l = {
            components: {
                ListVue: function ListVue() {
                    n.e("pages/cleaningtasks/com/list").then(function() {
                        return resolve(n("2dd8"));
                    }.bind(null, n)).catch(n.oe);
                }
            },
            data: function data() {
                return {
                    duration: 800,
                    index: 0,
                    pages: 1,
                    auntList: [],
                    last_page: null,
                    currentIndex: 0,
                    activeTab: 0,
                    tabList: [ {
                        name: "待处理",
                        type: 0
                    }, {
                        name: "已完成",
                        type: 1
                    }, {
                        name: "已取消",
                        type: 2
                    } ]
                };
            },
            computed: f(f({}, (0, o.mapGetters)([ "storeList", "userInfo" ])), {}, {
                activeStore: function activeStore() {
                    var e = this.storeList[this.currentIndex];
                    return e || {
                        name: "未选择",
                        id: -1
                    };
                }
            }),
            methods: {
                clearSuccess: function clearSuccess() {
                    this.resetRefTarget();
                },
                getRefTarget: function getRefTarget(e) {
                    var t = "list_".concat(e), n = this.$refs[t];
                    if (n && n.length) {
                        var r = (0, c.default)(n, 1), a = r[0];
                        return a;
                    }
                },
                triggerUpdate: function triggerUpdate(e) {
                    var t = this.getRefTarget(e);
                    t && (t.hasInit && !t.clear || t.initData());
                },
                swiperChange: function swiperChange(e) {
                    this.activeTab = e.detail.current, this.triggerUpdate(this.activeTab);
                },
                resetRefTarget: function resetRefTarget() {
                    for (var e = this.tabList.length, t = 0; t < e; t++) {
                        var n = this.getRefTarget(t);
                        if (!n) return;
                        n.clear = !0, t === this.activeTab && n.initData();
                    }
                },
                changeStore: function changeStore(e) {
                    var t = this;
                    return (0, i.default)(a.default.mark(function n() {
                        return a.default.wrap(function(n) {
                            while (1) switch (n.prev = n.next) {
                              case 0:
                                return t.currentIndex = 1 * e.detail.value, n.next = 3, t.$delay(300);

                              case 3:
                                t.resetRefTarget();

                              case 4:
                              case "end":
                                return n.stop();
                            }
                        }, n);
                    }))();
                },
                clickTab: function clickTab(e) {
                    var t = e.index;
                    this.activeTab !== t && (this.activeTab = t);
                }
            }
        };
        t.default = l;
    },
    aa01: function aa01(e, t, n) {
        "use strict";
        n.r(t);
        var r = n("9661"), a = n.n(r);
        for (var i in r) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(i);
        t["default"] = a.a;
    },
    b072: function b072(e, t, n) {
        "use strict";
        (function(e, t) {
            var r = n("4ea4");
            n("f9fc");
            r(n("66fd"));
            var a = r(n("e026"));
            e.__webpack_require_UNI_MP_PLUGIN__ = n, t(a.default);
        }).call(this, n("bc2e")["default"], n("543d")["createPage"]);
    },
    be83: function be83(e, t, n) {
        "use strict";
        var r = n("c114"), a = n.n(r);
        a.a;
    },
    c114: function c114(e, t, n) {},
    e026: function e026(e, t, n) {
        "use strict";
        n.r(t);
        var r = n("60df"), a = n("aa01");
        for (var i in a) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return a[e];
            });
        }(i);
        n("be83");
        var c = n("f0c5"), u = Object(c["a"])(a["default"], r["b"], r["c"], !1, null, "1180e5b5", null, !1, r["a"], void 0);
        t["default"] = u.exports;
    }
}, [ [ "b072", "common/runtime", "common/vendor" ] ] ]);