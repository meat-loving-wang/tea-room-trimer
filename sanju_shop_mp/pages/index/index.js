(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "pages/index/index" ], {
    4222: function _(t, e, r) {
        "use strict";
        r.d(e, "b", function() {
            return o;
        }), r.d(e, "c", function() {
            return i;
        }), r.d(e, "a", function() {
            return n;
        });
        var n = {
            uniLoadMore: function uniLoadMore() {
                return Promise.all([ r.e("common/vendor"), r.e("uni_modules/uni-load-more/components/uni-load-more/uni-load-more") ]).then(r.bind(null, "4b27"));
            },
            uPicker: function uPicker() {
                return Promise.all([ r.e("common/vendor"), r.e("uni_modules/uview-ui/components/u-picker/u-picker") ]).then(r.bind(null, "2aa2"));
            }
        }, o = function o() {
            var t = this, e = t.$createElement, r = (t._self._c, t.topBarTop());
            t._isMounted || (t.e0 = function(e) {
                t.showStroe = !0;
            }, t.e1 = function(e) {
                t.showStroe = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    m0: r
                }
            });
        }, i = [];
    },
    "61ef": function ef(t, e, r) {
        "use strict";
        var n = r("83a7"), o = r.n(n);
        o.a;
    },
    "826f": function f(t, e, r) {
        "use strict";
        r.r(e);
        var n = r("e19a"), o = r.n(n);
        for (var i in n) [ "default" ].indexOf(i) < 0 && function(t) {
            r.d(e, t, function() {
                return n[t];
            });
        }(i);
        e["default"] = o.a;
    },
    "83a7": function a7(t, e, r) {},
    c729: function c729(t, e, r) {
        "use strict";
        r.r(e);
        var n = r("4222"), o = r("826f");
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(t) {
            r.d(e, t, function() {
                return o[t];
            });
        }(i);
        r("61ef");
        var a = r("f0c5"), s = Object(a["a"])(o["default"], n["b"], n["c"], !1, null, null, null, !1, n["a"], void 0);
        e["default"] = s.exports;
    },
    d876: function d876(t, e, r) {
        "use strict";
        (function(t, e) {
            var n = r("4ea4");
            r("f9fc");
            n(r("66fd"));
            var o = n(r("c729"));
            t.__webpack_require_UNI_MP_PLUGIN__ = r, e(o.default);
        }).call(this, r("bc2e")["default"], r("543d")["createPage"]);
    },
    e19a: function e19a(t, e, r) {
        "use strict";
        (function(t) {
            var n = r("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = n(r("2eee")), i = n(r("278c")), a = n(r("c973")), s = n(r("9523")), u = r("26cb");
            function c(t, e) {
                var r = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), r.push.apply(r, n);
                }
                return r;
            }
            function d(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var r = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? c(Object(r), !0).forEach(function(e) {
                        (0, s.default)(t, e, r[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(r)) : c(Object(r)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(r, e));
                    });
                }
                return t;
            }
            var f = {
                data: function data() {
                    return {
                        showStroe: !1,
                        showType: t.getStorageSync("userinfo").type ? t.getStorageSync("userinfo").type : "",
                        page: 1,
                        last_page: 0,
                        orderList: [],
                        todayOrderNum: 0,
                        CanceOrderNum: 0,
                        paySuccessOrderNum: 0,
                        completeOrdeNum: 0,
                        storeName: "软程科技",
                        status: "more",
                        orderData: {}
                    };
                },
                computed: d(d(d({}, (0, u.mapState)({
                    storeList_store: function storeList_store(t) {
                        return t.storeList;
                    }
                })), (0, u.mapGetters)([ "activeStore", "token", "userInfo", "hasLogin" ])), {}, {
                    storeList: function storeList() {
                        var t = this.storeList_store;
                        return [ t ];
                    }
                }),
                onShow: function onShow() {
                    var e = this;
                    this.initOrderData();
                    var r = t.getStorageSync("should_update");
                    r && (setTimeout(function() {
                        var r = t.getStorageSync("userinfo");
                        if (r && r.hasOwnProperty("type")) {
                            var n = r.type;
                            e.showType = n;
                        }
                        1 == e.showType && (e.getOrderList(), e.getOrderInit());
                    }, 1e3), t.removeStorageSync("should_update"));
                },
                onLoad: function onLoad() {
                    var e = t.getStorageSync("userinfo");
                    if (e && e.hasOwnProperty("type")) {
                        var r = e.type;
                        this.showType = r;
                    }
                    1 == this.showType && (this.getOrderList(), this.getOrderInit());
                },
                onReachBottom: function onReachBottom() {
                    this.status = "noMore", this.page < this.last_page && (this.status = "more", this.page += 1, 
                    this.getOrderList());
                },
                methods: {
                    toDetail: function toDetail(e) {
                        var r = e.id;
                        t.navigateTo({
                            url: "/pages/order-detail/order-detail?id=" + r
                        });
                    },
                    earlyClose: function earlyClose(e) {
                        var r = this;
                        t.showModal({
                            title: "温馨提示",
                            content: "您确定要提前结束吗?",
                            success: function() {
                                var t = (0, a.default)(o.default.mark(function t(n) {
                                    var i, a;
                                    return o.default.wrap(function(t) {
                                        while (1) switch (t.prev = t.next) {
                                          case 0:
                                            if (!n.cancel) {
                                                t.next = 2;
                                                break;
                                            }
                                            return t.abrupt("return");

                                          case 2:
                                            i = e.id, a = r.userInfo.mobile, r.https("/order/earlyClose", {
                                                order_id: i,
                                                mobile: a
                                            }).then(function(t) {
                                                var e = t.code, n = t.msg;
                                                r.$toast({
                                                    title: n
                                                }), 1 === e && (r.page = 1, r.getOrderList());
                                            }).catch(function(t) {
                                                r.$toast({
                                                    title: t.msg
                                                });
                                            });

                                          case 5:
                                          case "end":
                                            return t.stop();
                                        }
                                    }, t);
                                }));
                                return function(e) {
                                    return t.apply(this, arguments);
                                };
                            }()
                        });
                    },
                    reorderHandle: function reorderHandle(e) {
                        var r = this;
                        t.showModal({
                            title: "温馨提示",
                            content: "您确定要退款吗?",
                            success: function() {
                                var t = (0, a.default)(o.default.mark(function t(n) {
                                    var i, a;
                                    return o.default.wrap(function(t) {
                                        while (1) switch (t.prev = t.next) {
                                          case 0:
                                            if (!n.cancel) {
                                                t.next = 2;
                                                break;
                                            }
                                            return t.abrupt("return");

                                          case 2:
                                            i = e.id, a = r.userInfo.mobile, r.https("/order/ordertuikuan", {
                                                order_id: i,
                                                mobile: a
                                            }).then(function(t) {
                                                var e = t.code, n = t.msg;
                                                r.$toast({
                                                    title: n
                                                }), 1 === e && (r.page = 1, r.getOrderList());
                                            }).catch(function(t) {
                                                r.$toast({
                                                    title: t.msg
                                                });
                                            });

                                          case 5:
                                          case "end":
                                            return t.stop();
                                        }
                                    }, t);
                                }));
                                return function(e) {
                                    return t.apply(this, arguments);
                                };
                            }()
                        });
                    },
                    initOrderData: function initOrderData() {
                        var t = this;
                        return (0, a.default)(o.default.mark(function e() {
                            var r, n, i, a, s, u;
                            return o.default.wrap(function(e) {
                                while (1) switch (e.prev = e.next) {
                                  case 0:
                                    if (r = t.token, n = t.activeStore, r.length) {
                                        e.next = 3;
                                        break;
                                    }
                                    return e.abrupt("return");

                                  case 3:
                                    if (-1 !== n.id) {
                                        e.next = 5;
                                        break;
                                    }
                                    return e.abrupt("return", t.$toast({
                                        title: "未选择店铺"
                                    }));

                                  case 5:
                                    return i = t.userInfo.mobile, e.next = 8, t.https("/shujutongji/indextongji", {
                                        store_id: n.id,
                                        mobile: i
                                    });

                                  case 8:
                                    if (a = e.sent, s = a.code, u = a.data, a.msg, 1 === s && u) {
                                        e.next = 12;
                                        break;
                                    }
                                    return e.abrupt("return");

                                  case 12:
                                    t.orderData = u, console.log(a, "orderData");

                                  case 14:
                                  case "end":
                                    return e.stop();
                                }
                            }, e);
                        }))();
                    },
                    selectItem: function selectItem(e) {
                        var r = (0, i.default)(e.value, 1), n = r[0], o = n.id;
                        t.setStorageSync("storeListId", o), this.$store.commit("setActiveId", o), this.showStroe = !1, 
                        this.page = 1, this.getOrderList(), this.getOrderInit();
                    },
                    goLogin: function goLogin() {
                        t.navigateTo({
                            url: "../login/login"
                        });
                    },
                    getOrderList: function getOrderList() {
                        var t = this, e = this.orderList, r = this.activeStore;
                        -1 !== r.id && this.https("/order/orderList", {
                            page: this.page,
                            store_id: r.id
                        }).then(function(r) {
                            t.last_page = r.data.last_page, 1 == t.page ? t.orderList = r.data.data : t.orderList = e.concat(r.data.data);
                        });
                    },
                    getOrderInit: function getOrderInit() {
                        this.getTodayOrderNum(), this.getCanceOrderNum(), this.getPaySuccessOrderNum(), 
                        this.getCompleteOrdeNum();
                    },
                    getTodayOrderNum: function getTodayOrderNum() {
                        var t = this, e = this.activeStore;
                        -1 !== e.id && this.https("/order/todayOrder", {
                            store_id: e.id
                        }).then(function(e) {
                            t.todayOrderNum = e.data;
                        });
                    },
                    getCanceOrderNum: function getCanceOrderNum() {
                        var t = this, e = this.activeStore;
                        -1 !== e.id && this.https("/order/cancelOrder", {
                            store_id: e.id
                        }).then(function(e) {
                            t.CanceOrderNum = e.data;
                        });
                    },
                    getPaySuccessOrderNum: function getPaySuccessOrderNum() {
                        var t = this, e = this.activeStore;
                        -1 !== e.id && this.https("/order/payOrder", {
                            store_id: e.id
                        }).then(function(e) {
                            console.log(e), t.paySuccessOrderNum = e.data;
                        });
                    },
                    getCompleteOrdeNum: function getCompleteOrdeNum() {
                        var t = this, e = this.activeStore;
                        -1 !== e.id && this.https("/order/completeOrder", {
                            store_id: e.id
                        }).then(function(e) {
                            t.completeOrdeNum = e.data;
                        });
                    }
                }
            };
            e.default = f;
        }).call(this, r("543d")["default"]);
    }
}, [ [ "d876", "common/runtime", "common/vendor" ] ] ]);