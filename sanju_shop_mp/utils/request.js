const httpsUrl = "https://sanju.xinyunweb.com/apiadmin"
function request ({url, data, method="POST"}) {
	data = data || {};
	data.token = wx.getStorageSync('token') || '';
	data.app_id =  10001;
	return new Promise((resolve, reject) => {
		wx.request({
			url: httpsUrl + url,
			data: data,
			dataType: 'json',
			method: method,
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			success: (res) => {
				if (res.statusCode !== 200 || typeof res.data !== 'object') {
				return	reject(res)
				}
				if (res.data.code === -1) {
					// 登录态失效, 重新登录
					reject(res.data)
					return wx.navigateTo({
						url: '/pages/login/login'
					})
				}
				resolve(res.data)
			},
			fail: (res) => {
				reject(res.data)
			},
		})
	})
}

module.exports ={
    request,
}