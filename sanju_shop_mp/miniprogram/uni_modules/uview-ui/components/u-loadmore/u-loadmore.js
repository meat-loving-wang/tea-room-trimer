(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "uni_modules/uview-ui/components/u-loadmore/u-loadmore" ], {
    "0ad0": function ad0(t, n, o) {
        "use strict";
        (function(t) {
            var e = o("4ea4");
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var i = e(o("850c")), u = {
                name: "u-loadmore",
                mixins: [ t.$u.mpMixin, t.$u.mixin, i.default ],
                data: function data() {
                    return {
                        dotText: "●"
                    };
                },
                computed: {
                    loadTextStyle: function loadTextStyle() {
                        return {
                            color: this.color,
                            fontSize: t.$u.addUnit(this.fontSize),
                            lineHeight: t.$u.addUnit(this.fontSize),
                            backgroundColor: this.bgColor
                        };
                    },
                    showText: function showText() {
                        var t = "";
                        return t = "loadmore" == this.status ? this.loadmoreText : "loading" == this.status ? this.loadingText : "nomore" == this.status && this.isDot ? this.dotText : this.nomoreText, 
                        t;
                    }
                },
                methods: {
                    loadMore: function loadMore() {
                        "loadmore" == this.status && this.$emit("loadmore");
                    }
                }
            };
            n.default = u;
        }).call(this, o("543d")["default"]);
    },
    "0bbb": function bbb(t, n, o) {
        "use strict";
        o.r(n);
        var e = o("34a2"), i = o("dfed");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            o.d(n, t, function() {
                return i[t];
            });
        }(u);
        o("5e88");
        var a = o("f0c5"), d = Object(a["a"])(i["default"], e["b"], e["c"], !1, null, "0f91d4e1", null, !1, e["a"], void 0);
        n["default"] = d.exports;
    },
    "0d5f": function d5f(t, n, o) {},
    "34a2": function a2(t, n, o) {
        "use strict";
        o.d(n, "b", function() {
            return i;
        }), o.d(n, "c", function() {
            return u;
        }), o.d(n, "a", function() {
            return e;
        });
        var e = {
            uLine: function uLine() {
                return Promise.all([ o.e("common/vendor"), o.e("uni_modules/uview-ui/components/u-line/u-line") ]).then(o.bind(null, "ecb7"));
            },
            uLoadingIcon: function uLoadingIcon() {
                return Promise.all([ o.e("common/vendor"), o.e("uni_modules/uview-ui/components/u-loading-icon/u-loading-icon") ]).then(o.bind(null, "027a"));
            }
        }, i = function i() {
            var t = this, n = t.$createElement, o = (t._self._c, t.__get_style([ t.$u.addStyle(t.customStyle), {
                backgroundColor: t.bgColor,
                marginBottom: t.$u.addUnit(t.marginBottom),
                marginTop: t.$u.addUnit(t.marginTop),
                height: t.$u.addUnit(t.height)
            } ])), e = t.__get_style([ t.loadTextStyle ]);
            t.$mp.data = Object.assign({}, {
                $root: {
                    s0: o,
                    s1: e
                }
            });
        }, u = [];
    },
    "5e88": function e88(t, n, o) {
        "use strict";
        var e = o("0d5f"), i = o.n(e);
        i.a;
    },
    dfed: function dfed(t, n, o) {
        "use strict";
        o.r(n);
        var e = o("0ad0"), i = o.n(e);
        for (var u in e) [ "default" ].indexOf(u) < 0 && function(t) {
            o.d(n, t, function() {
                return e[t];
            });
        }(u);
        n["default"] = i.a;
    }
} ]);

(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ "uni_modules/uview-ui/components/u-loadmore/u-loadmore-create-component", {
    "uni_modules/uview-ui/components/u-loadmore/u-loadmore-create-component": function uni_modulesUviewUiComponentsULoadmoreULoadmoreCreateComponent(module, exports, __webpack_require__) {
        __webpack_require__("543d")["createComponent"](__webpack_require__("0bbb"));
    }
}, [ [ "uni_modules/uview-ui/components/u-loadmore/u-loadmore-create-component" ] ] ]);