require("../../../@babel/runtime/helpers/Arrayincludes");

(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "pages/cleaningtasks/com/list" ], {
    "150e": function e(t, _e, n) {},
    "2dd8": function dd8(t, e, n) {
        "use strict";
        n.r(e);
        var a = n("87f1"), o = n("bc8c");
        for (var s in o) [ "default" ].indexOf(s) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(s);
        n("543b");
        var i = n("f0c5"), u = Object(i["a"])(o["default"], a["b"], a["c"], !1, null, "44f90dca", null, !1, a["a"], void 0);
        e["default"] = u.exports;
    },
    "543b": function b(t, e, n) {
        "use strict";
        var a = n("150e"), o = n.n(a);
        o.a;
    },
    "62ba": function ba(t, e, n) {
        "use strict";
        (function(t) {
            var a = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = a(n("2eee")), s = a(n("448a")), i = a(n("c973")), u = {
                props: [ "type", "storeId", "mobile", "auto" ],
                mounted: function mounted() {
                    this.auto && !this.hasInit && this.initData();
                },
                methods: {
                    sureClaerRoom: function sureClaerRoom(e, n) {
                        var a = this;
                        t.showModal({
                            title: "温馨提示",
                            content: "确认".concat(n, "房间已打扫过了吗？"),
                            success: function success(n) {
                                n.cancel || a.https("/order/editStatus", {
                                    id: e,
                                    aunt_mobile: t.getStorageSync("userinfo").mobile
                                }).then(function() {
                                    a.$emit("clearSuccess");
                                }).catch(function(e) {
                                    t.showToast({
                                        title: e.msg,
                                        icon: "none"
                                    });
                                });
                            }
                        });
                    },
                    initData: function initData() {
                        var e = this;
                        return (0, i.default)(o.default.mark(function n() {
                            var a, i, u, r, c;
                            return o.default.wrap(function(n) {
                                while (1) switch (n.prev = n.next) {
                                  case 0:
                                    if (e.clear && (e.page = 1, e.list = [], e.isEmpty = !1, e.status = "loadmore", 
                                    e.clear = !1, e.hasInit = !1), ![ "nomore", "loading" ].includes(e.status)) {
                                        n.next = 3;
                                        break;
                                    }
                                    return n.abrupt("return");

                                  case 3:
                                    return e.status = "loading", a = e.page, i = e.type, u = e.mobile, r = e.storeId, 
                                    console.log("storeId", r), n.next = 8, e.$delay(300);

                                  case 8:
                                    c = {
                                        page: a,
                                        mobile: u,
                                        status: i,
                                        store_id: r
                                    }, console.log("请求参数==>", c), e.https("/order/auntChargelists", c).then(function(t) {
                                        var n, o = t.code, i = t.data;
                                        if (o < 1) return e.hasInit = !0, e.isEmpty = !e.list.length, e.status = "nomore";
                                        var u = i.data, r = i.last_page;
                                        console.log(u, "list"), u && u.length && (n = e.list).push.apply(n, (0, s.default)(u)), 
                                        e.isEmpty = !e.list.length;
                                        var c = a < r;
                                        if (e.hasInit = !0, !c) return e.status = "nomore";
                                        e.status = "loadmore", e.page += 1;
                                    }).catch(function(n) {
                                        n.data;
                                        var a = n.msg;
                                        e.hasInit = !0, e.status = "nomore", e.isEmpty = !e.list.length, a && t.showModal({
                                            title: "警告",
                                            content: a,
                                            showCancel: !1
                                        });
                                    });

                                  case 11:
                                  case "end":
                                    return n.stop();
                                }
                            }, n);
                        }))();
                    }
                },
                computed: {},
                data: function data() {
                    return {
                        clear: !1,
                        page: 1,
                        list: [],
                        isEmpty: !1,
                        status: "loadmore",
                        hasInit: !1
                    };
                }
            };
            e.default = u;
        }).call(this, n("543d")["default"]);
    },
    "87f1": function f1(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return s;
        }), n.d(e, "a", function() {
            return a;
        });
        var a = {
            uEmpty: function uEmpty() {
                return Promise.all([ n.e("common/vendor"), n.e("uni_modules/uview-ui/components/u-empty/u-empty") ]).then(n.bind(null, "f8f8"));
            },
            uLoadmore: function uLoadmore() {
                return Promise.all([ n.e("common/vendor"), n.e("uni_modules/uview-ui/components/u-loadmore/u-loadmore") ]).then(n.bind(null, "0bbb"));
            }
        }, o = function o() {
            var t = this.$createElement;
            this._self._c;
        }, s = [];
    },
    bc8c: function bc8c(t, e, n) {
        "use strict";
        n.r(e);
        var a = n("62ba"), o = n.n(a);
        for (var s in a) [ "default" ].indexOf(s) < 0 && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(s);
        e["default"] = o.a;
    }
} ]);

(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ "pages/cleaningtasks/com/list-create-component", {
    "pages/cleaningtasks/com/list-create-component": function pagesCleaningtasksComListCreateComponent(module, exports, __webpack_require__) {
        __webpack_require__("543d")["createComponent"](__webpack_require__("2dd8"));
    }
}, [ [ "pages/cleaningtasks/com/list-create-component" ] ] ]);