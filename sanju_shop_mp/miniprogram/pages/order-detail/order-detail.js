(global["webpackJsonp"] = global["webpackJsonp"] || []).push([ [ "pages/order-detail/order-detail" ], {
    "513d": function d(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return a;
        }), n.d(e, "c", function() {
            return r;
        }), n.d(e, "a", function() {});
        var a = function a() {
            var t = this.$createElement;
            this._self._c;
        }, r = [];
    },
    "7cd4": function cd4(t, e, n) {
        "use strict";
        (function(t) {
            var a = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var r = a(n("2eee")), o = a(n("c973")), c = {
                onLoad: function onLoad(t) {
                    var e = t.id;
                    this.initData(e), this.id = e;
                },
                computed: {
                    showCopyBtn: function showCopyBtn() {
                        var t = this.order;
                        return !!t || t.length > 0;
                    }
                },
                methods: {
                    callPhone: function callPhone() {
                        var e = this.order.mobile;
                        t.showModal({
                            title: "提示",
                            content: "您确定要拨打 ".concat(e, " 吗?"),
                            success: function success(n) {
                                n.cancel || t.makePhoneCall({
                                    phoneNumber: e
                                });
                            }
                        });
                    },
                    copyHandle: function copyHandle() {
                        var e = this, n = this.order.mobile;
                        t.setClipboardData({
                            data: n,
                            success: function success() {
                                e.$toast({
                                    title: "复制成功"
                                });
                            }
                        });
                    },
                    initData: function initData(t) {
                        var e = this;
                        return (0, o.default)(r.default.mark(function n() {
                            var a, o;
                            return r.default.wrap(function(n) {
                                while (1) switch (n.prev = n.next) {
                                  case 0:
                                    return n.next = 2, e.https("/order/orderDetail", {
                                        order_id: t
                                    });

                                  case 2:
                                    a = n.sent, a.code, o = a.data, a.msg, e.order = o, console.log(a, "resp");

                                  case 6:
                                  case "end":
                                    return n.stop();
                                }
                            }, n);
                        }))();
                    }
                },
                data: function data() {
                    return {
                        reorderList: [],
                        order: {}
                    };
                }
            };
            e.default = c;
        }).call(this, n("543d")["default"]);
    },
    "8cb8": function cb8(t, e, n) {
        "use strict";
        var a = n("d59d"), r = n.n(a);
        r.a;
    },
    "9ea8": function ea8(t, e, n) {
        "use strict";
        n.r(e);
        var a = n("7cd4"), r = n.n(a);
        for (var o in a) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(o);
        e["default"] = r.a;
    },
    d59d: function d59d(t, e, n) {},
    e7d5: function e7d5(t, e, n) {
        "use strict";
        n.r(e);
        var a = n("513d"), r = n("9ea8");
        for (var o in r) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(o);
        n("8cb8");
        var c = n("f0c5"), i = Object(c["a"])(r["default"], a["b"], a["c"], !1, null, "61ff59ac", null, !1, a["a"], void 0);
        e["default"] = i.exports;
    },
    fa19: function fa19(t, e, n) {
        "use strict";
        (function(t, e) {
            var a = n("4ea4");
            n("f9fc");
            a(n("66fd"));
            var r = a(n("e7d5"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(r.default);
        }).call(this, n("bc2e")["default"], n("543d")["createPage"]);
    }
}, [ [ "fa19", "common/runtime", "common/vendor" ] ] ]);